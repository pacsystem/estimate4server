package net.diaowen.common.utils;

import java.util.regex.Pattern;

public class testOK {
    public static boolean checkDateFormat(String timeStr){
        String rexp = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
        return Pattern.matches(rexp, timeStr);
    }
    public static void main(String[] args)
    {
        String timeStr = "2021-08-17 13:47:28";

        boolean test = checkDateFormat(timeStr);
        System.out.println(test);
    }

}
