/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.diaowen.common.plugs.security;

import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.estimate.common.RoleCode;
import net.diaowen.estimate.service.UserManager;
import org.apache.shiro.authc.*;
import org.apache.shiro.authc.credential.HashedCredentialsMatcher;
import org.apache.shiro.authc.credential.PasswordMatcher;
import org.apache.shiro.authc.credential.SimpleCredentialsMatcher;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.SimplePrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

public class NotLoginRealm extends AuthorizingRealm {

	@Autowired
	protected AccountManager accountManager;
	@Autowired
	private UserManager userManager;
	@Value("${initPwd}")
	private String initPwd;

	public NotLoginRealm() {
		setCredentialsMatcher(new SimpleCredentialsMatcher());
	}

	/**
	 * 认证回调函数,登录时调用.
	 */
	@Override
	protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authcToken) throws AuthenticationException {
		UsernamePasswordToken token = (UsernamePasswordToken) authcToken;

		User user = accountManager.findUserByLoginNameOrEmail(token.getUsername());
		if(user==null && new String(token.getPassword()).startsWith("!@#notLogin")){
			//当是免登录时的用户才自动新增
			user = new User();
			user.setLoginName(token.getUsername());
			user.setPwd(initPwd);
			user.setStatus(2);
			user.setRoleId("9");
			userManager.adminSave(user);
		}
		return new SimpleAuthenticationInfo(token.getUsername(), "!@#notLogin"+token.getUsername() , getName());
	}

	/**
	 * 授权查询回调函数, 进行鉴权但缓存中无用户的授权信息时调用.
	 */
	@Override
	protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
		String username = principals.toString();
//		User user = accountManager.findUserByLoginName(username);
		User user = accountManager.findUserByLoginNameOrEmail(username);
		if(user!=null){
			SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
			if ("1".equals(user.getId()) || "9".equals(user.getRoleId())) {
				info.addRole(RoleCode.DWSURVEY_SUPER_ADMIN);
			}
			return info;
		}
		return null;
	}


}
