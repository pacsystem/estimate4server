package net.diaowen.estimate.controller.question;

import com.octo.captcha.service.image.ImageCaptchaService;
import net.diaowen.common.QuType;
import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.ipaddr.IPService;
import net.diaowen.common.plugs.web.Token;
import net.diaowen.common.plugs.zxing.ZxingUtil;
import net.diaowen.common.utils.CookieUtils;
import net.diaowen.common.utils.NumberUtils;
import net.diaowen.common.utils.TimeUtils;
import net.diaowen.estimate.common.AnswerCheckData;
import net.diaowen.estimate.config.DWSurveyConfig;
import net.diaowen.estimate.dao.SurveyAnswerDao;
import net.diaowen.estimate.entity.*;
import net.diaowen.estimate.service.CommonDataDictManager;
import net.diaowen.estimate.service.SurveyAnswerManager;
import net.diaowen.estimate.service.SurveyDirectoryManager;
import net.diaowen.estimate.service.SysOrgManager;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.WebUtils;

import javax.imageio.ImageIO;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

/**
 * 答卷 action
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
@Controller
@RequestMapping("/api/dwsurvey/anon/response")
public class ResponseController {
	private static final long serialVersionUID = -2289729314160067840L;
	//private static String DATE_REGEX = "^([1-9]\\d{3}-)(([0]{0,1}[1-9]-)|([1][0-2]-))(([0-3]{0,1}[0-9]))$";
	private static String DATE_REGEX = "^((([0-9]{3}[1-9]|[0-9]{2}[1-9][0-9]{1}|[0-9]{1}[1-9][0-9]{2}|[1-9][0-9]{3})-(((0[13578]|1[02])-(0[1-9]|[12][0-9]|3[01]))|((0[469]|11)-(0[1-9]|[12][0-9]|30))|(02-(0[1-9]|[1][0-9]|2[0-8]))))|((([0-9]{2})(0[48]|[2468][048]|[13579][26])|((0[48]|[2468][048]|[3579][26])00))-02-29))\\s+([0-1]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])$";
	@Autowired
	private SurveyAnswerManager surveyAnswerManager;
	@Autowired
	private SurveyDirectoryManager directoryManager;
	@Autowired
	private IPService ipService;
	@Autowired
	private AccountManager accountManager;
	@Autowired
	private ImageCaptchaService imageCaptchaService;

	@Autowired
	private SysOrgManager sysOrgManager;

	@Autowired
	private SurveyAnswerDao surveyAnswerDao;
    @Autowired
    private CommonDataDictManager commonDataDictManager;

//	@Autowired
//	private SurveyAnswerLogManager surveyAnswerLogManager;

	@RequestMapping("/save.do")
	public String save(HttpServletRequest request,HttpServletResponse response
			,String siteUrl
			,String surveyId
			,String estimateName
			,String estimateGender
			,String estimateAge
			,String outpatientNumber
			,String hospitalNumber
			,String inspectNumber
			,String source
			,String sourceNumber
			,String orgName
			,String orgCode
			,String estimateTemplateName
			,String estimateDate
			,String estimatePerson
			,String estimateScore
			,String estimateStatus
			,String answerId
            ,String businessName
			,String isEdit
	) throws Exception {
		return saveSurvey(request,response
				,siteUrl
				,surveyId
				,estimateName
				,estimateGender
				,estimateAge
				,outpatientNumber
				,hospitalNumber
				,inspectNumber
				,source
				,sourceNumber
				,orgName
				,orgCode
				,estimateTemplateName
				,estimateDate
				,estimatePerson
				,estimateScore
				,estimateStatus
				,answerId
                ,businessName
				,isEdit);
	}

	@RequestMapping("/saveMobile.do")
	public String saveMobile(HttpServletRequest request,HttpServletResponse response
			,String siteUrl
			,String surveyId
			,String estimateName
			,String estimateGender
			,String estimateAge
			,String outpatientNumber
			,String hospitalNumber
			,String inspectNumber
			,String source
			,String sourceNumber
			,String orgName
			,String orgCode
			,String estimateTemplateName
			,String estimateDate
			,String estimatePerson
			,String estimateScore
			,String estimateStatus
			) throws Exception {
		return saveSurvey(request,response
				,siteUrl
				,surveyId
				,estimateName
				,estimateGender
				,estimateAge
				,outpatientNumber
				,hospitalNumber
				,inspectNumber
				,source
				,sourceNumber
				,orgName
				,orgCode
				,estimateTemplateName
				,estimateDate
				,estimatePerson
				,estimateScore
				,estimateStatus
				,null, null,null);
	}

	public String saveSurvey(HttpServletRequest request,HttpServletResponse response
			,String siteUrl
			,String surveyId
			,String estimateName
			,String estimateGender
			,String estimateAge
			,String outpatientNumber
			,String hospitalNumber
			,String inspectNumber
			,String source
			,String sourceNumber
			,String orgName
			,String orgCode
			,String estimateTemplateName
			,String estimateDate
			,String estimatePerson
			,String estimateScore
			,String estimateStatus
			,String answerId
            ,String businessName
			,String isEdit
	) throws Exception {
		SurveyDirectory directory = null;
		try {
			directory = directoryManager.getSurvey(surveyId);

			SurveyAnswer entity=new SurveyAnswer();

			entity.setBusinessTypeId(directory.getBusinessTypeId());
			entity.setAppraise(directory.getAppraise());

			if(StringUtils.isNotEmpty(answerId))
			{
				entity.setAnswerId(answerId);
			}
			else
			{
				entity.setAnswerId(directory.getId());
			}

			entity.setAnswerId(surveyId);
			if(StringUtils.isNotEmpty(estimateName))
			{
				entity.setEstimateName(estimateName);
			}
			if(StringUtils.isNotEmpty(estimateGender))
			{
				entity.setEstimateGender(estimateGender);
			}
			if(StringUtils.isNotEmpty(estimateAge))
			{
				entity.setEstimateAge(estimateAge);
			}
			if(StringUtils.isNotEmpty(outpatientNumber))
			{
				entity.setOutpatientNumber(outpatientNumber);
			}
			if(StringUtils.isNotEmpty(hospitalNumber))
			{
				entity.setHospitalNumber(hospitalNumber);
			}
			if(StringUtils.isNotEmpty(inspectNumber))
			{
				entity.setInspectNumber(inspectNumber);
			}
			if(StringUtils.isNotEmpty(source))
			{
				entity.setSource(source);
			}
			if(StringUtils.isNotEmpty(sourceNumber))
			{
				entity.setSourceNumber(sourceNumber);
			}

			if(StringUtils.isNotEmpty(orgName))
			{
				entity.setOrgName(orgName);
			}

			if(StringUtils.isNotEmpty(orgCode))
			{
				entity.setOrgCode(orgCode);
			}

			if(StringUtils.isNotEmpty(estimateTemplateName))
			{
				entity.setEstimateTemplateName(estimateTemplateName);
			}
			else
			{
				entity.setEstimateTemplateName(directory.getSurveyName());
			}

			if(StringUtils.isNotEmpty(sourceNumber))
			{
				entity.setSourceNumber(sourceNumber);
			}

			if(StringUtils.isNotEmpty(estimateDate))
			{
				boolean matches =  Pattern.matches(DATE_REGEX, estimateDate);
				Date _date = new Date();
				if(matches == true){
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
					//设置日期格式转的严谨性
					sdf.setLenient(false);
					try {
						_date = sdf.parse(estimateDate);
					} catch (ParseException e) {
						_date = new Date();
					}
				}
				entity.setEstimateDate(_date);
			}
			else
			{
				entity.setEstimateDate(new Date());
			}

			if(StringUtils.isNotEmpty(estimatePerson))
			{
				entity.setEstimatePerson(estimatePerson);
			}

			if(StringUtils.isNotEmpty(estimateScore))
			{
				entity.setEstimateScore(estimateScore);
			}

			if(StringUtils.isNotEmpty(businessName))
			{
				entity.setBusinessName(businessName);
			}

			AnswerCheckData answerCheckData = answerCheckData(request,directory, true, entity);
			if(!answerCheckData.isAnswerCheck())
				return answerRedirect(siteUrl,directory,answerCheckData.getAnswerCheckCode());

			checkOrcreateSysOrg(orgCode,orgName,estimatePerson);

			if(StringUtils.isNotEmpty(isEdit) && isEdit.equals("true")){
				SurveyAnswer answer = null;
				if(org.apache.commons.lang3.StringUtils.isNotEmpty(entity.getSourceNumber())) {
					answer = surveyAnswerDao.findBysourceNumber(entity.getSourceNumber());
				}
				else if (org.apache.commons.lang3.StringUtils.isNotEmpty(entity.getAnswerId())) {
					answer = surveyAnswerManager.findById(entity.getAnswerId());
				}
				if(answer!=null) {
					surveyAnswerDao.updateAnswerAnResult(answer.getId());
				}
			}

			answerAfterUpData(request,response,surveyId,entity.getId());

			answerSurvey(request,surveyId,entity);

			return answerRedirect(siteUrl,directory,6, entity.getId());
		} catch (Exception e) {
			e.printStackTrace();
			return answerRedirect(siteUrl,directory,5);
		}
	}
	public void checkOrcreateSysOrg(String orgCode,String orgName,String createBy){
		Criterion cri = Restrictions.eq("orgCode",orgCode);
		List<SysOrg> listSysOrg = sysOrgManager.findList(cri);
		if((null== listSysOrg) || (listSysOrg.size()==0))
		{
			SysOrg sysOrg = new SysOrg();
			sysOrg.setOrgCode(orgCode);
			sysOrg.setOrgName(orgName);
			sysOrg.setCreateBy(createBy);
			sysOrg.setCreateTime(new Date());
			sysOrg.setStatus("0");
			sysOrgManager.save(sysOrg);
		}
	}
	public AnswerCheckData answerCheckData(HttpServletRequest request, SurveyDirectory directory,boolean isSubmit,SurveyAnswer entity) {
		AnswerCheckData answerCheckData = new AnswerCheckData();
		String surveyId = directory.getId();
		SurveyDetail surveyDetail = directory.getSurveyDetail();
		Integer answerNum = directory.getAnswerNum();
		Integer visibility = directory.getVisibility();
		Integer effective = surveyDetail.getEffective();
		Integer rule = surveyDetail.getRule();
		Integer refresh = surveyDetail.getRefresh();
		Integer refreshNum = surveyDetail.getRefreshNum();
		Integer ynEndNum = surveyDetail.getYnEndNum();
		Integer endNum = surveyDetail.getEndNum();
		Integer ynEndTime = surveyDetail.getYnEndTime();
		Date endTime = surveyDetail.getEndTime();
		////答卷设置是否可以重复答卷
		Integer congelation=surveyDetail.getCongelation();
		if(visibility!=1){
			answerCheckData.setAnswerCheck(false);
			answerCheckData.setAnswerCheckCode(10);//问卷已经删除
		}
		if (directory.getSurveyQuNum()<=0 || directory.getSurveyState() != 1 ) {
			answerCheckData.setAnswerCheck(false);
			answerCheckData.setAnswerCheckCode(1);//问卷未开启
		}

		//1、每台电脑或手机只能答一次, cookie
//		Cookie cookie = CookieUtils.getCookie(request, surveyId);
//		Integer cookieAnNum = 0;
//		if(effective!=null && effective>1 && cookie!=null){
//			//effective cookie
//			String cookieValue = cookie.getValue();
//			if (cookieValue != null && NumberUtils.isNumeric(cookieValue)) {
//				cookieAnNum = Integer.parseInt(cookieValue);
//			}
//			if(cookieAnNum>0){
//				answerCheckData.setAnswerCheck(false);
//				answerCheckData.setAnswerCheckCode(3);//超过cookie次数限制
//				//跳转到结束提示
//				return answerCheckData;
//			}
//		}

		String ip = ipService.getIp(request);
		//2、每个IP只能答一次，N次，IP
//		Integer effectiveIp = surveyDetail.getEffectiveIp();
//		if (effectiveIp != null && effectiveIp == 1) {
//			Long ipAnNum = surveyAnswerManager.getCountByIp(surveyId, ip);
//			if(ipAnNum!=null && ipAnNum > 0){
//				answerCheckData.setAnswerCheck(false);
//				answerCheckData.setAnswerCheckCode(23);//超过单个IP次数限制
//				return answerCheckData;
//			}
//		}



		String ruleCode = request.getParameter("ruleCode");
		//4、拥有口令密码才可以答题，可用次数，口令码
		if(rule!=null && rule==3){
			boolean isPwdCode = false;
			if(StringUtils.isNotEmpty(ruleCode)) {
				if(!ruleCode.equals(surveyDetail.getRuleCode())){
					//code不正确
//					modelAndView.addObject("redType", 302);//code不正确
					answerCheckData.setAnswerCheck(false);
					answerCheckData.setAnswerCheckCode(302);//口令错误
					return answerCheckData;
				}
			}
			if(StringUtils.isEmpty(ruleCode)) {
				answerCheckData.setAnswerCheck(false);
				answerCheckData.setAnswerCheckCode(303);//口令为空
			}
		}



		//8、收集量
		if(answerNum!=null && ynEndNum==1 && answerNum >= endNum ){
			answerCheckData.setAnswerCheck(false);
			answerCheckData.setAnswerCheckCode(7);
			return answerCheckData;
		}

		//5、有重复回答启用验证码 cookie
		if(refresh!=null && refresh==1){
			Cookie refCookie = CookieUtils.getCookie(request, "r_"+surveyId);
			if (refCookie!=null) {
				//cookie启用验证码
				answerCheckData.setImgCheckCode(true);
			}
		}
        //判断答卷是否冻结
		String estimateStatus="0";
        if(congelation==0)
		{
			estimateStatus="1";
			directoryManager.saveByAdmin(directory);
			answerCheckData.setAnswerCheck(false);
			answerCheckData.setAnswerCheckCode(11); //不支持修改
		}
		HttpSession httpSession = request.getSession();
		if(isSubmit){
			String refCookieKey = "r_"+surveyId;
			Cookie refCookie = CookieUtils.getCookie(request, refCookieKey);
			if (( refresh==1 && refCookie!=null)) {
				String code = request.getParameter("jcaptchaInput");
//				if(code!=null) {
//					if (!imageCaptchaService.validateResponseForID(request.getSession().getId(), code)) {
////					return "redirect:/response/answer-estimate.do?surveyId="+surveyId+"&redType=4";
////				return answerRedirect(directory,4);
//						answerCheckData.setAnswerCheck(false);
//						answerCheckData.setAnswerCheckCode(4);
//						return answerCheckData;
//					}
//				}
			}
			//设置entity的数据
			if(answerCheckData.isAnswerCheck()){

				User user = accountManager.getCurUser();
				if (user != null) {
					entity.setUserId(user.getId());
				}

				entity.setIpAddr(ip);
				entity.setSurveyId(surveyId);
				entity.setEstimateStatus(estimateStatus);
			}
		}
		return answerCheckData;
	}

	public void answerSurvey(HttpServletRequest request,String surveyId,SurveyAnswer entity){
		Map<String, Map<String, Object>> quMaps = buildSaveSurveyMap(request);
		String bgTimeAttrName = "bgTime"+surveyId;
		Date bgAnTime = (Date)request.getSession().getAttribute(bgTimeAttrName);
		entity.setBgAnDate(bgAnTime);
		entity.setEndAnDate(new Date());
		surveyAnswerManager.saveAnswer(entity, quMaps);
		//更新答卷分数
		Map<String,Object> scoreMaps=quMaps.get("scoreMaps");
		String surveyAnswerId=entity.getId();
		if(scoreMaps!=null){
			for (String key : scoreMaps.keySet()) {
				String quId=key;
				Map<String,Object> mapRows=(Map<String, Object>) scoreMaps.get(key);
				boolean isAn = false;
				for (String keyRow : mapRows.keySet()) {
					String rowId=keyRow;
					String scoreValue=mapRows.get(keyRow).toString();
					if(scoreValue!=null && !"".equals(scoreValue)){
						//判断是不是总评分题 同时更新答卷分数
						surveyAnswerDao.upanswer_estimatescore(quId,surveyAnswerId);
					}
				}
			}
		}
	}

	public void answerAfterUpData(HttpServletRequest request, HttpServletResponse response, String surveyId,String answerId) {
		SurveyDirectory directory = directoryManager.getSurvey(surveyId);
		SurveyDetail surveyDetail = directory.getSurveyDetail();
		Integer answerNum = directory.getAnswerNum();
		Integer ynEndNum = surveyDetail.getYnEndNum();
		Integer endNum = surveyDetail.getEndNum();
		Integer ynEndTime = surveyDetail.getYnEndTime();
		Date endTime = surveyDetail.getEndTime();

		int effe = surveyDetail.getEffectiveTime();
		effe = 24;
		String refCookieKey = "r_"+surveyId;
		CookieUtils.addCookie(response, surveyId, (1) + "",effe * 60, "/");
		CookieUtils.addCookie(response, refCookieKey, (1) + "", null, "/");

		//7、何时结束，结束时间
		if(endTime!=null && ynEndTime==1 &&  (new Date().getTime()-endTime.getTime()) > 0 ){
			directory.setSurveyState(2);
			directoryManager.saveByAdmin(directory);
		}

		if(answerNum!=null && ynEndNum==1 && answerNum >= endNum ){
			directory.setSurveyState(2);
			directoryManager.saveByAdmin(directory);
		}

		String surveyLogId = request.getParameter("surveyLogId");
//		surveyAnswerLogManager.upSurveyLogAnswerId(surveyLogId, answerId);

	}

	public Map<String, Map<String, Object>> buildSaveSurveyMap(HttpServletRequest request) {
		Map<String, Map<String, Object>> quMaps = new HashMap<String, Map<String, Object>>();
		Map<String, Object> yesnoMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.YESNO + "_");
		quMaps.put("yesnoMaps", yesnoMaps);
		Map<String, Object> radioMaps = WebUtils.getParametersStartingWith(
				request, "qu_"+QuType.RADIO + "_");
		Map<String, Object> checkboxMaps = WebUtils.getParametersStartingWith(
				request, "qu_"+QuType.CHECKBOX + "_");
		Map<String, Object> fillblankMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.FILLBLANK + "_");
		quMaps.put("fillblankMaps", fillblankMaps);
		Map<String, Object> dfillblankMaps = WebUtils
				.getParametersStartingWith(request, "qu_"
						+ QuType.MULTIFILLBLANK + "_");
		for (String key : dfillblankMaps.keySet()) {
			String dfillValue = dfillblankMaps.get(key).toString();
			Map<String, Object> map = WebUtils.getParametersStartingWith(
					request, dfillValue);
			dfillblankMaps.put(key, map);
		}
		quMaps.put("multifillblankMaps", dfillblankMaps);
		Map<String, Object> answerMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.ANSWER + "_");
		quMaps.put("answerMaps", answerMaps);
		Map<String, Object> compRadioMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.COMPRADIO + "_");
		for (String key : compRadioMaps.keySet()) {
			String enId = key;
			String quItemId = compRadioMaps.get(key).toString();
			String otherText = request.getParameter("text_qu_"
					+ QuType.COMPRADIO + "_" + enId + "_" + quItemId);
			AnRadio anRadio = new AnRadio();
			anRadio.setQuId(enId);
			anRadio.setQuItemId(quItemId);
			anRadio.setOtherText(otherText);
			compRadioMaps.put(key, anRadio);
		}
		quMaps.put("compRadioMaps", compRadioMaps);
		Map<String, Object> compCheckboxMaps = WebUtils
				.getParametersStartingWith(request, "qu_" + QuType.COMPCHECKBOX
						+ "_");//复合多选
		for (String key : compCheckboxMaps.keySet()) {
			String dfillValue = compCheckboxMaps.get(key).toString();
			Map<String, Object> map = WebUtils.getParametersStartingWith(
					request, "tag_" + dfillValue);
			for (String key2 : map.keySet()) {
				String quItemId = map.get(key2).toString();
				String otherText = request.getParameter("text_"
						+ dfillValue + quItemId);
				AnCheckbox anCheckbox = new AnCheckbox();
				anCheckbox.setQuItemId(quItemId);
				anCheckbox.setOtherText(otherText);
				map.put(key2, anCheckbox);
			}
			compCheckboxMaps.put(key, map);
		}
		quMaps.put("compCheckboxMaps", compCheckboxMaps);
		Map<String, Object> enumMaps = WebUtils.getParametersStartingWith(request, "qu_" + QuType.ENUMQU + "_");//枚举
		quMaps.put("enumMaps", enumMaps);
		Map<String, Object> quOrderMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.ORDERQU + "_");//排序
		for (String key : quOrderMaps.keySet()) {
			String tag = quOrderMaps.get(key).toString();
			Map<String, Object> map = WebUtils.getParametersStartingWith(
					request, tag);
			quOrderMaps.put(key, map);
		}
		quMaps.put("quOrderMaps", quOrderMaps);
		for (String key:radioMaps.keySet()) {
			String enId = key;
			String quItemId = radioMaps.get(key).toString();
			String otherText = request.getParameter("text_qu_"
					+ QuType.RADIO + "_" + enId + "_" + quItemId);
			AnRadio anRadio = new AnRadio();
			anRadio.setQuId(enId);
			anRadio.setQuItemId(quItemId);
			anRadio.setOtherText(otherText);
			radioMaps.put(key, anRadio);
		}
		// 评分题
		Map<String, Object> scoreMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.SCORE + "_");
		for (String key : scoreMaps.keySet()) {
			String tag = scoreMaps.get(key).toString();
			Map<String, Object> map = WebUtils.getParametersStartingWith(
					request, tag);
			scoreMaps.put(key, map);
		}
		quMaps.put("scoreMaps", scoreMaps);
		quMaps.put("compRadioMaps", radioMaps);
		for (String key : checkboxMaps.keySet()) {
			String dfillValue = checkboxMaps.get(key).toString();
			Map<String, Object> map = WebUtils.getParametersStartingWith(
					request, dfillValue);
			for (String key2 : map.keySet()) {
				String quItemId = map.get(key2).toString();
				String otherText = request.getParameter("text_"
						+ dfillValue + quItemId);
				AnCheckbox anCheckbox = new AnCheckbox();
				anCheckbox.setQuItemId(quItemId);
				anCheckbox.setOtherText(otherText);
				map.put(key2, anCheckbox);
			}
			checkboxMaps.put(key, map);
		}
		quMaps.put("compCheckboxMaps", checkboxMaps);

		Map<String, Object> uploadFileMaps = WebUtils.getParametersStartingWith(
				request, "qu_" + QuType.UPLOADFILE + "_");//填空
		quMaps.put("uploadFileMaps", uploadFileMaps);

		return quMaps;
	}

	public String answerRedirect(String siteUrl,SurveyDirectory directory, int redType) throws Exception {
		return answerRedirect(siteUrl,directory,redType,null);
	}

	public String answerRedirect(String siteUrl,SurveyDirectory directory, int redType, String answerId) throws Exception {
		if((StringUtils.isEmpty(siteUrl)) || (null==siteUrl)) {
			if (directory != null) {
				return "redirect:" + DWSurveyConfig.DWSURVEY_WEB_SITE_URL + "/static/diaowen/message.html?sid=" + directory.getSid() + "&respType=" + redType + "&answerId=" + answerId;
			}
			return "redirect:" + DWSurveyConfig.DWSURVEY_WEB_SITE_URL + "/static/diaowen/message.html";
		}
		else
		{
			if (directory != null) {
				return "redirect:" + siteUrl + "/static/diaowen/message.html?sid=" + directory.getSid() + "&respType=" + redType + "&answerId=" + answerId;
			}
			return "redirect:" + siteUrl + "/static/diaowen/message.html";
		}
	}


	@RequestMapping("/answer-dwsurvey.do")
	public ModelAndView answerRedirect(HttpServletRequest request,String surveyId, int redType) throws Exception {
		ModelAndView modelAndView = new ModelAndView();
		SurveyDirectory directory = directoryManager.getSurvey(surveyId);
		modelAndView.addObject("survey", directory);
		modelAndView.addObject("redType", redType);
		modelAndView.setViewName("/diaowen-answer/response-msg-1");
		return modelAndView;
	}

	/**
	 * 答卷异步有效性验
	 * @return
	 */
	@RequestMapping("/check-answer-survey.do")
	@ResponseBody
	public HttpResult checkAnswerSurvey(HttpServletRequest request,HttpServletResponse response,String surveyId){
		//0、token 访止重复提交
		String token = Token.getToken(request);
		AnswerCheckData answerCheckData = answerCheckData(request,surveyId);
		answerCheckData.setToken(token);
		return HttpResult.SUCCESS(answerCheckData);
	}

	public AnswerCheckData answerCheckData(HttpServletRequest request, String surveyId){
		SurveyDirectory directory = directoryManager.getSurvey(surveyId);

		return answerCheckData(request,directory, false, null);
	}


	/**
	 * 获取问卷详情
	 * @return
	 */
	@RequestMapping(value = "/survey.do")
	public String survey(HttpServletRequest request, HttpServletResponse response, String sid,String surveyId) {
		try {
			if(StringUtils.isEmpty(sid) && StringUtils.isNotEmpty(surveyId)){
				SurveyDirectory surveyDirectory = directoryManager.get(surveyId);
				sid = surveyDirectory.getSid();
                if (surveyDirectory!=null){
                    CommonDataDict dict = commonDataDictManager.getCommonDataDictById(surveyDirectory.getAppraise());
                    if(dict != null){
                        surveyDirectory.setAppraiseName(dict.getItemName());
                    }
                }
			}
			String jsonPath = "/file/survey/"+sid+"/"+sid+".json";
			surveyJsonExists(sid, jsonPath);
			request.getRequestDispatcher(jsonPath).forward(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}


	@RequestMapping(value = "/survey_info.do")
	public String surveyInfo(HttpServletRequest request, HttpServletResponse response, String sid) {
		String jsonPath = "/file/survey/"+sid+"/"+sid+"_info.json";
		try {
			surveyJsonExists(sid, jsonPath);
			request.getRequestDispatcher(jsonPath).forward(request,response);
		} catch (ServletException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private void surveyJsonExists(String sid, String jsonPath) {
		//判断有无没有则生成一个
		String filePath = DWSurveyConfig.DWSURVEY_WEB_FILE_PATH+ jsonPath;
		filePath = filePath.replace("/",File.separator);
		filePath = filePath.replace("\\",File.separator);
		File file = new File(filePath);
		if(!file.exists()){
			//不存在则生成一个
			SurveyDirectory directory = directoryManager.getSurveyBySid(sid);
			directoryManager.devSurveyJson(directory.getId());
		}
	}

	//回答问卷的二维码
	@RequestMapping("/answerTD.do")
	public String answerTD(HttpServletRequest request,HttpServletResponse response,String surveyId,String sid) throws Exception{
		String WEB_SITE_URL = DWSurveyConfig.DWSURVEY_WEB_SITE_URL;
		String down=request.getParameter("down");
		String ruleCode = request.getParameter("ruleCode");
		String baseUrl = "";
		baseUrl = request.getScheme() +"://" + request.getServerName()
				+ (request.getServerPort() == 80 ? "" : ":" +request.getServerPort())
				+ request.getContextPath();
		baseUrl = WEB_SITE_URL;
//		String encoderContent= baseUrl+"/response/answerMobile.do?surveyId="+surveyId;
		String encoderContent = null;
		if(StringUtils.isNotEmpty(surveyId)){
			encoderContent = baseUrl+"/static/diaowen/answer-m.html?surveyId="+surveyId;
		}
		if(StringUtils.isNotEmpty(sid)){
			encoderContent = baseUrl+"/static/diaowen/answer-m.html?sid="+sid;
		}
		if(StringUtils.isNotEmpty(ruleCode)){
			encoderContent+="&ruleCode="+ruleCode;
		}
		ByteArrayOutputStream jpegOutputStream = new ByteArrayOutputStream();
		BufferedImage twoDimensionImg = ZxingUtil.qRCodeCommon(encoderContent, "jpg", 16);
		ImageIO.write(twoDimensionImg, "jpg", jpegOutputStream);
		if(down==null){
			response.setHeader("Cache-Control", "no-store");
			response.setHeader("Pragma", "no-cache");
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			ServletOutputStream responseOutputStream = response.getOutputStream();
			responseOutputStream.write(jpegOutputStream.toByteArray());
			responseOutputStream.flush();
			responseOutputStream.close();
		}else{
			response.addHeader("Content-Disposition", "attachment;filename=" + new String(("diaowen_"+surveyId+".jpg").getBytes()));
			byte[] bys = jpegOutputStream.toByteArray();
			response.addHeader("Content-Length", "" + bys.length);
			ServletOutputStream responseOutputStream = response.getOutputStream();
			response.setContentType("application/octet-stream");
			responseOutputStream.write(bys);
			responseOutputStream.flush();
			responseOutputStream.close();
		}
		return null;
	}


	@RequestMapping("/getOrgByCode.do")
	@ResponseBody
	public SysOrg getOrgByCode(HttpServletRequest request,HttpServletResponse response,String orgCode){
		return sysOrgManager.getOrgByCode(orgCode);
	}
}
