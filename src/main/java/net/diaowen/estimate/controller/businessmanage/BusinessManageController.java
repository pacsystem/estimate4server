package net.diaowen.estimate.controller.businessmanage;


import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.httpclient.PageResult;
import net.diaowen.common.plugs.httpclient.ResultUtils;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.estimate.entity.BusinessManage;
import net.diaowen.estimate.service.BusinessManageManager;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/dwsurvey/app/businessmanage")
public class BusinessManageController {

    @Autowired
    private BusinessManageManager businessManageManager;

    @Autowired
    private AccountManager accountManager;

    /**
     * 拉取业务类型列表
     * @param pageResult
     * @param status
     * @param businessName
     * @return
     */
    @RequestMapping(value = "/list.do",method = RequestMethod.GET)
    @ResponseBody
    public PageResult<BusinessManage> list(PageResult<BusinessManage> pageResult,Integer status, String businessName) {

        BusinessManage businessManage = new BusinessManage();
        if(StringUtils.isNotBlank(businessName))
        {
            businessManage.setBusinessName(businessName);
        }
        if(status!=null)
        {
            businessManage.setDataStatus(status);
        }

        User user = accountManager.getCurUser();
        if(user!=null){
            Page page = ResultUtils.getPageByPageResult(pageResult);
            page = businessManageManager.findPage(page);
            page = businessManageManager.findPage(page, businessManage);
            pageResult = ResultUtils.getPageResultByPage(page,pageResult);
        }
        return pageResult;
    }

    /**
     * 添加
     * @param businessManage
     * @return
     */
    @RequestMapping(value = "/add.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult add(@RequestBody BusinessManage businessManage) {
        try{
            BusinessManage result = businessManageManager.SaveBusinessManage(businessManage);
            if(result!=null) return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
            return HttpResult.FAILURE(e.getMessage());
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 修改
     * @param businessManage
     * @return
     */
    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult edit(@RequestBody BusinessManage businessManage) {
        try{
            BusinessManage result = businessManageManager.UpdateBusinessManage(businessManage);
            if(result!=null) return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
            return HttpResult.FAILURE(e.getMessage());
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 物理删除IDS
     * @return
     */
    @RequestMapping(value = "/delete.do",method = RequestMethod.DELETE)
    @ResponseBody
    public HttpResult delete(@RequestBody Map<String, String[]> map) {
        try{
            if(map!=null){
                if(map.containsKey("id")){
                    String[] ids = map.get("id");
                    if(ids!=null){
                        //todo:当评价总表增加业务类型后，增加业务类型下是否有相应的评价，如果有，不能删除
                        businessManageManager.deleteData(ids);
                    }
                }
            }
            return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
        }

        return HttpResult.SUCCESS("false");
    }

    /**
     * 逻辑删除IDS
     * @return
     */
    @RequestMapping(value = "/disable.do",method = RequestMethod.PUT)
    @ResponseBody
    public HttpResult disable(@RequestBody Map<String, String[]> map) {
        try{
            if(map!=null){
                if(map.containsKey("id")){
                    String[] ids = map.get("id");
                    if(ids!=null){
                        //todo:当评价总表增加业务类型后，增加业务类型下是否有相应的评价，如果有，不能删除
                        businessManageManager.disableData(ids);
                    }
                }
            }
            return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 检测是否存在
     * @param businessManage
     * @return
     */
    @RequestMapping(value = "/check-exists.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult checkExists(@RequestBody BusinessManage businessManage) {
        try {
            boolean result = businessManageManager.checkBusinessManageExists(businessManage);
            if (result == false)
                return HttpResult.SUCCESS("true");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 拉取业务类型列表
     * @return
     */
    @RequestMapping(value = "/nvmenulist.do",method = RequestMethod.GET)
    @ResponseBody
    public List<BusinessManage> nvmenulist() {
        List<BusinessManage> list = businessManageManager.getNvMenuList();
        return list;
    }

}
