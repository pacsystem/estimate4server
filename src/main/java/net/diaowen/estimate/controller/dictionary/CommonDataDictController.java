package net.diaowen.estimate.controller.dictionary;


import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.httpclient.PageResult;
import net.diaowen.common.plugs.httpclient.ResultUtils;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.estimate.entity.BusinessManage;
import net.diaowen.estimate.entity.CommonDataDict;
import net.diaowen.estimate.service.CommonDataDictManager;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/dwsurvey/app/dictionary")
public class CommonDataDictController {

    @Autowired
    private CommonDataDictManager commonDataDictManager;

    @Autowired
    private AccountManager accountManager;

    /**
     * 拉取业务类型列表
     * @param pageResult
     * @param dataStatus
     * @param itemCode
     * @param itemName
     * @param dictParentId
     * @return
     */
    @RequestMapping(value = "/list.do",method = RequestMethod.GET)
    @ResponseBody
    public PageResult<CommonDataDict> list(PageResult<CommonDataDict> pageResult,
                                           String itemName,
                                           String itemCode,
                                           Integer dataStatus,
                                           String dictParentId
                                           ) {

        CommonDataDict commonDataDict = new CommonDataDict();
        if(StringUtils.isNotBlank(itemName))
        {
            commonDataDict.setItemName(itemName);
        }

        if(StringUtils.isNotBlank(itemCode))
        {
            commonDataDict.setItemCode(itemCode);
        }

        if(StringUtils.isNotBlank(dictParentId))
        {
            commonDataDict.setDictParentId(dictParentId);
        }

        if(dataStatus!=null)
        {
            commonDataDict.setDataStatus(dataStatus);
        }

        User user = accountManager.getCurUser();
        if(user!=null){
            Page page = ResultUtils.getPageByPageResult(pageResult);
            page = commonDataDictManager.findPage(page, commonDataDict);
            pageResult = ResultUtils.getPageResultByPage(page,pageResult);
        }
        return pageResult;
    }

    /**
     * 拉取业务类型列表
     * @return
     */
    @RequestMapping(value = "/getDataDictItemByCode.do",method = RequestMethod.GET)
    @ResponseBody
    public List<CommonDataDict> getDataDictItemByCode(String itemCode) {

        List<CommonDataDict> list = commonDataDictManager.getDataDictItemByCode(itemCode);
        return list;
    }


    /**
     * 添加
     * @param commonDataDict
     * @return
     */
    @RequestMapping(value = "/add.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult add(@RequestBody CommonDataDict commonDataDict) {
        try{
            CommonDataDict result = commonDataDictManager.saveCommonDataDict(commonDataDict);
            if(result!=null) return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
            return HttpResult.FAILURE(e.getMessage());
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 修改
     * @param commonDataDict
     * @return
     */
    @RequestMapping(value = "/edit.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult edit(@RequestBody CommonDataDict commonDataDict) {
        try{
            CommonDataDict result = commonDataDictManager.updateCommonDataDict(commonDataDict);
            if(result!=null) return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
            return HttpResult.FAILURE(e.getMessage());
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 物理删除IDS
     * @return
     */
    @RequestMapping(value = "/delete.do",method = RequestMethod.DELETE)
    @ResponseBody
    public HttpResult delete(@RequestBody Map<String, String[]> map) {
        try{
            if(map!=null){
                if(map.containsKey("id")){
                    String[] ids = map.get("id");
                    if(ids!=null){
                        //todo:当评价总表增加业务类型后，增加业务类型下是否有相应的评价，如果有，不能删除
                        commonDataDictManager.deleteData(ids);
                    }
                }
            }
            return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
        }

        return HttpResult.SUCCESS("false");
    }

    /**
     * 逻辑删除IDS
     * @return
     */
    @RequestMapping(value = "/disable.do",method = RequestMethod.PUT)
    @ResponseBody
    public HttpResult disable(@RequestBody Map<String, String[]> map) {
        try{
            if(map!=null){
                if(map.containsKey("id")){
                    String[] ids = map.get("id");
                    if(ids!=null){
                        //todo:当评价总表增加业务类型后，增加业务类型下是否有相应的评价，如果有，不能删除
                        commonDataDictManager.disableData(ids);
                    }
                }
            }
            return HttpResult.SUCCESS("true");
        }catch (Exception e){
            e.printStackTrace();
        }
        return HttpResult.SUCCESS("false");
    }

    /**
     * 检测是否存在
     * @param commonDataDict
     * @return
     */
    @RequestMapping(value = "/check-exists.do",method = RequestMethod.POST)
    @ResponseBody
    public HttpResult checkExists(@RequestBody CommonDataDict commonDataDict) {
        try {
            boolean result = commonDataDictManager.checkCommonDataDictExists(commonDataDict);
            if (result == false)
                return HttpResult.SUCCESS("true");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.SUCCESS("false");
    }
}
