package net.diaowen.estimate.controller.sys;

import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.httpclient.PageResult;
import net.diaowen.common.plugs.httpclient.ResultUtils;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.estimate.entity.SysOrg;
import net.diaowen.estimate.service.SysOrgManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/api/dwsurvey/app/org")
public class SysOrgController {

    @Autowired
    private SysOrgManager sysOrgManager;

    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    public PageResult<SysOrg> list(PageResult<SysOrg> pageResult, String orgName) {
        Page page = ResultUtils.getPageByPageResult(pageResult);
        page = sysOrgManager.findPage(page, orgName);
        pageResult = ResultUtils.getPageResultByPage(page, pageResult);
        return pageResult;
    }

    @RequestMapping(value = "/add.do", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult add(@RequestBody SysOrg sysOrg) {
        try {
            Integer flag = sysOrgManager.check(sysOrg.getOrgName(), sysOrg.getOrgCode());
            if (flag != 0) {
                return HttpResult.SUCCESS(flag);
            }
            SysOrg result = sysOrgManager.orgCreate(sysOrg);
            if (result != null) return HttpResult.SUCCESS();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }

    @RequestMapping(value = "/edit.do", method = RequestMethod.PUT)
    @ResponseBody
    public HttpResult update(@RequestBody SysOrg sysOrg) {
        try {
            Integer flag = sysOrgManager.check(sysOrg.getOrgName(), sysOrg.getOrgCode());
            if (flag != 0) {
                return HttpResult.SUCCESS(flag);
            }
            HttpResult httpResult = sysOrgManager.orgUpdate(sysOrg);
            return httpResult;
        } catch (Exception e) {
            e.printStackTrace();
            return HttpResult.FAILURE(e.getMessage());
        }
    }

    @RequestMapping(value = "/delete.do", method = RequestMethod.DELETE)
    @ResponseBody
    public HttpResult delete(@RequestBody Map<String, String[]> map) {
        try {
            if (map != null) {
                if (map.containsKey("id")) {
                    String[] ids = map.get("id");
                    if (ids != null) {
                        sysOrgManager.orgDelete(ids);
                    }
                }
            }
            return HttpResult.SUCCESS();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }

    @RequestMapping(value = "/switchStatus.do", method = RequestMethod.GET)
    @ResponseBody
    public HttpResult switchStatus(String id) {
        try {
            sysOrgManager.switchStatus(id);
            return HttpResult.SUCCESS();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }
}
