package net.diaowen.estimate.controller.survey;

import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.httpclient.PageResult;
import net.diaowen.common.plugs.httpclient.ResultUtils;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.utils.UserAgentUtils;
import net.diaowen.common.utils.ZipUtil;
import net.diaowen.estimate.config.DWSurveyConfig;
import net.diaowen.estimate.dao.SurveyAnswerDao;
import net.diaowen.estimate.dao.SysOrgDao;
import net.diaowen.estimate.entity.*;
import net.diaowen.estimate.service.*;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/api/dwsurvey/app/answer")
public class MySurveyAnswerController {

    @Autowired
    private SurveyDirectoryManager surveyDirectoryManager;
    @Autowired
    private SurveyAnswerManager surveyAnswerManager;
    @Autowired
    private AccountManager accountManager;
    @Autowired
    private AnUploadFileManager anUploadFileManager;

    @Autowired
    private SysOrgDao sysOrgDao;

    @Autowired
    private SurveyAnswerDao surveyAnswerDao;

    @Autowired
    private CommonDataDictManager commonDataDictManager;


    /**
     * 获取答卷列表
     *
     * @return
     */
    @RequestMapping(value = "/list.do", method = RequestMethod.GET)
    @ResponseBody
    public PageResult survey(HttpServletRequest request, PageResult<SurveyAnswer> pageResult,
                             String businessId, String surveyId, String ipAddr
            , String city, Integer isEffective
            , String startToEndEstimateDate,String orgCode ,String estimateStatus
            , String businessName,String businessOrderCode,String estimatePerson
            , String source,String sourceNumber
            , String startToEndDate) {
        UserAgentUtils.userAgent(request);
        User user = accountManager.getCurUser();
        if (user != null) {
            Page page = ResultUtils.getPageByPageResult(pageResult);
            SurveyDirectory survey = surveyDirectoryManager.get(surveyId);
            if (survey != null) {
                if (!user.getId().equals(survey.getUserId())) {
                    pageResult.setSuccess(false);
                    return pageResult;
                }
                String[] surveyIds;
                if (StringUtils.isNotEmpty(surveyId)) {
                    surveyIds = new String[]{surveyId};
                } else {
                    surveyIds = surveyDirectoryManager.getSurveyByType(businessId).stream().map(SurveyDirectory::getId).toArray(String[]::new);
                }
                Map<String,String> params = new HashMap<>();
                params.put("startToEndDate",startToEndDate);
                params.put("businessId",businessId);
                params.put("surveyId",surveyId);
                params.put("startToEndEstimateDate",startToEndEstimateDate);
                params.put("orgCode",orgCode);
                params.put("estimateStatus",estimateStatus);
                params.put("businessName",businessName);
                params.put("businessOrderCode",businessOrderCode);
                params.put("estimatePerson",estimatePerson);
                params.put("source",source);
                params.put("sourceNumber",sourceNumber);

                page = surveyAnswerManager.answerPage(page, surveyIds, params);
            }
            pageResult = ResultUtils.getPageResultByPage(page, pageResult);
        }
        return pageResult;

    }

    /**
     * 获取所有答卷列表
     *
     * @return
     */
    @RequestMapping(value = "/listAll.do", method = RequestMethod.GET)
    @ResponseBody
    public PageResult surveyAll(HttpServletRequest request, PageResult<SurveyAnswer> pageResult,
                                String businessId, String surveyId, String ipAddr
            ,String city, Integer isEffective
            , String startToEndEstimateDate,String orgCode ,String estimateStatus
            , String businessName,String businessOrderCode, String estimatePerson
            ,String source,String sourceNumber
            ,String startToEndDate) {
        UserAgentUtils.userAgent(request);

        Page page = ResultUtils.getPageByPageResult(pageResult);
        //如果需要用户验证再打开
//            SurveyDirectory survey=surveyDirectoryManager.get(surveyId);
//            if(survey!=null){
//                if(!user.getId().equals(survey.getUserId())){
//                    pageResult.setSuccess(false);
//                    return pageResult;
//                }
//                page=surveyAnswerManager.answerPage(page, surveyId);
//            }
        String[] surveyIds;
        if (StringUtils.isNotEmpty(surveyId)) {
            surveyIds = new String[]{surveyId};
        } else {
            surveyIds = surveyDirectoryManager.getSurveyByType(businessId).stream().map(SurveyDirectory::getId).toArray(String[]::new);
        }
        Map<String,String> params = new HashMap<>();
        params.put("startToEndDate",startToEndDate);
        params.put("businessId",businessId);
        params.put("surveyId",surveyId);
        params.put("startToEndEstimateDate",startToEndEstimateDate);
        params.put("orgCode",orgCode);
        params.put("estimateStatus",estimateStatus);
        params.put("businessName",businessName);
        params.put("businessOrderCode",businessOrderCode);
        params.put("estimatePerson",estimatePerson);
        params.put("source",source);
        params.put("sourceNumber",sourceNumber);

        page = surveyAnswerManager.answerPage(page, surveyIds, params);

        List<SurveyAnswer> surveyAnswers = page.getResult();
        for (int i = 0; i < surveyAnswers.size(); i++) {
            String _SurveyId = surveyAnswers.get(i).getSurveyId();
            SurveyDirectory _survey = surveyDirectoryManager.get(_SurveyId);
            if (_survey != null) {
                surveyAnswers.get(i).setSurveyName(_survey.getSurveyName());
            } else {
                surveyAnswers.get(i).setSurveyName(surveyAnswers.get(i).getSurveyId());
            }
        }

        page.setResult(surveyAnswers);

        pageResult = ResultUtils.getPageResultByPage(page, pageResult);

        return pageResult;

    }


    @RequestMapping(value = "/info.do", method = RequestMethod.POST)
    @ResponseBody
    public HttpResult info(@RequestBody SurveyAnswer surr) throws Exception {
        try {
            SurveyAnswer answer = null;
            if(StringUtils.isNotEmpty(surr.getSourceNumber())) {
                answer = surveyAnswerDao.findBysourceNumber(surr.getSourceNumber());
            }
            else if (StringUtils.isNotEmpty(surr.getAnswerId())) {
                answer = surveyAnswerManager.findById(surr.getAnswerId());
            }
            if (answer != null) {
                SurveyDirectory survey = surveyDirectoryManager.findUniqueBy(answer.getSurveyId());
                if (survey!=null){
                    CommonDataDict dict = commonDataDictManager.getCommonDataDictById(survey.getAppraise());
                    if(dict != null){
                        survey.setAppraiseName(dict.getItemName());
                    }
                }
                List<Question> questions = surveyAnswerManager.findAnswerDetail(answer);
                survey.setQuestions(questions);
                survey.setSurveyAnswer(answer);
                return HttpResult.SUCCESS(survey);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }

    /**
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/delete.do", method = RequestMethod.DELETE)
    @ResponseBody
    public HttpResult delete(@RequestBody Map<String, String[]> map) throws Exception {
//		HttpResult httpResultRP = ShiroAuthorizationUtils.isDefaultAdminRoleAndPermissionHttpResult(PermissionCode.HT_CASCADEDB_DELETE);
//		if (httpResultRP != null) return httpResultRP;
        try {
            if (map != null) {
                if (map.containsKey("id")) {
                    String[] ids = map.get("id");
                    if (ids != null) {
                        surveyAnswerManager.deleteData(ids);
                    }
                }
            }
            return HttpResult.SUCCESS();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }


    @RequestMapping("/export-xls.do")
    @ResponseBody
    public String exportXLS(HttpServletRequest request, HttpServletResponse response, String surveyId, String expUpQu,@RequestParam(value = "exportIds", required = false) List<String> exportIds) throws Exception {
        try {
            String savePath = DWSurveyConfig.DWSURVEY_WEB_FILE_PATH;
            User user = accountManager.getCurUser();
            if (user != null) {
                SurveyDirectory survey = surveyDirectoryManager.get(surveyId);
                if (survey != null) {
                    if (!user.getId().equals(survey.getUserId())) {
                        return "没有相应数据权限";
                    }
                    List<AnUplodFile> anUplodFiles = anUploadFileManager.findAnswer(surveyId);
                    if (anUplodFiles != null && anUplodFiles.size() > 0 && expUpQu != null && "1".equals(expUpQu)) {
                        //直接导出excel，不存在上传文件的问题
                        savePath = surveyAnswerManager.exportXLS(surveyId, savePath, true,exportIds);
                        //启用压缩导出
                        String fromPath = DWSurveyConfig.DWSURVEY_WEB_FILE_PATH + "/webin/expfile/" + surveyId;
                        fromPath = fromPath.replace("/", File.separator);

                        String zipPath = DWSurveyConfig.DWSURVEY_WEB_FILE_PATH + "/webin/zip/".replace("/", File.separator);
                        File file = new File(zipPath);
                        if (!file.exists())
                            file.mkdirs();

                        String toPath = zipPath + surveyId + ".zip";
                        toPath = toPath.replace("/", File.separator);
                        ZipUtil.createZip(fromPath, toPath, false);
                        response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode("dwsurvey_" + survey.getSid() + ".zip", "UTF-8"));
                        request.getRequestDispatcher("/webin/zip/" + surveyId + ".zip").forward(request, response);
                    } else {
                        //直接导出excel，不存在上传文件的问题
                        savePath = surveyAnswerManager.exportXLS(surveyId, savePath, false,exportIds);
                        response.setHeader("Content-Disposition", "attachment; filename=" + java.net.URLEncoder.encode("dwsurvey_" + survey.getSid() + ".xlsx", "UTF-8"));
                        request.getRequestDispatcher(savePath).forward(request, response);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 拉取组织无条件列表
     * @return
     * @throws Exception
     */
    @RequestMapping(value = "/orgSimpleList.do", method = RequestMethod.GET)
    @ResponseBody
    public HttpResult getOrgSimpleList() throws Exception {
        try {
            List<SysOrg> listSysOrg = sysOrgDao.orgSimpleList();
            return HttpResult.SUCCESS(listSysOrg);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return HttpResult.FAILURE();
    }

}
