package net.diaowen.estimate.service;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.SysSendEmail;

public interface SysSendEmailManager extends BaseService<SysSendEmail, String> {

	public void scanning();

}
