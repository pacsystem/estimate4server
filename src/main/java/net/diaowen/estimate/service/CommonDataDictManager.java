package net.diaowen.estimate.service;

import net.diaowen.common.plugs.page.Page;
import net.diaowen.estimate.entity.BusinessManage;
import net.diaowen.estimate.entity.CommonDataDict;

import java.util.List;

/**
 * 导入错误记录
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
public interface CommonDataDictManager {

    public Page<CommonDataDict> findPage(Page<CommonDataDict> page, CommonDataDict entity);

    List<CommonDataDict> getDataDictItemByCode(String itemCode);
    public CommonDataDict saveCommonDataDict(CommonDataDict entity);
    public CommonDataDict updateCommonDataDict(CommonDataDict entity);
    public boolean checkCommonDataDictExists(CommonDataDict entity);
    public void deleteData(String[] ids);
    public void disableData(String[] ids);
    CommonDataDict getCommonDataDictById(String id);
}
