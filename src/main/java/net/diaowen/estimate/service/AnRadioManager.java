package net.diaowen.estimate.service;

import java.util.List;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.AnRadio;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.DataCross;

/**
 * 单选题
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
public interface AnRadioManager extends BaseService<AnRadio, String>{
	public AnRadio findAnswer(String belongAnswerId, String quId);

	public void findGroupStats(Question question);

	public List<DataCross> findStatsDataCross(Question rowQuestion,
                                              Question colQuestion);

	public List<DataCross> findStatsDataChart(Question question);
}
