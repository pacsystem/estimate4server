package net.diaowen.estimate.service;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.SysDbBackup;

/**
 * 数据备份
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
public interface SysDbBackupManager extends BaseService<SysDbBackup, String>{

}
