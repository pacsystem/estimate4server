package net.diaowen.estimate.service;

import java.util.List;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.AnEnumqu;

/**
 * 枚举题
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
public interface AnEnumquManager extends BaseService<AnEnumqu, String> {
	public  List<AnEnumqu> findAnswer(String belongAnswerId, String quId);

	public void findGroupStats(Question question);
}
