package net.diaowen.estimate.service.impl;

import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.service.BaseServiceImpl;
import net.diaowen.estimate.dao.SysOrgDao;
import net.diaowen.estimate.entity.SysOrg;
import net.diaowen.estimate.service.SysOrgManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.ArrayList;
import java.util.List;



@Service
public class SysOrgManagerImpl extends BaseServiceImpl<SysOrg, String> implements SysOrgManager {

    @Autowired
    private SysOrgDao sysOrgDao;

    @Override
    public void setBaseDao() {
        this.baseDao = sysOrgDao;
    }

    @Override
    public Page<SysOrg> findPage(Page<SysOrg> page, String orgName) {
        List<Criterion> criterions = new ArrayList<Criterion>();
        if (StringUtils.isNotEmpty(orgName)) {
            criterions.add(Restrictions.like("orgName", "%" + orgName + "%"));
        }
        criterions.add(Restrictions.ne("status", "1"));
        if (StringUtils.isEmpty(page.getOrderBy())) {
            page.setOrderBy("createTime");
            page.setOrderDir("desc");
        }
        return super.findPageByCri(page, criterions);
    }

    @Override
    public SysOrg orgCreate(SysOrg entity) {
        super.save(entity);
        return entity;
    }

    @Transactional
    @Override
    public HttpResult orgUpdate(SysOrg entity) {
        if (entity != null) {
            String id = entity.getId();
            if (id != null) {
                SysOrg sysOrg = getModel(id);
                sysOrg.setOrgName(entity.getOrgName());
                sysOrg.setOrgCode(entity.getOrgCode());
                super.save(sysOrg);
                return HttpResult.SUCCESS();
            }
        }
        return HttpResult.FAILURE("user为null");
    }

    @Transactional
    @Override
    public void orgDelete(String[] ids) {
        for (String id : ids) {
            SysOrg sysOrg = get(id);
            sysOrg.setStatus("1");
            save(sysOrg);
        }
    }

    @Override
    public Integer check(String orgName, String orgCode) {

        if (StringUtils.isNoneBlank(orgName)) {
            Criterion criterion1 = Restrictions.eq("orgName", orgCode);
            Criterion criterion2= Restrictions.ne("status", "1");
            List<SysOrg> sysOrgList=sysOrgDao.find(criterion1,criterion2);
            if(sysOrgList.size() > 0) {
                return 1;
            }
        }
        if (StringUtils.isNoneBlank(orgCode)) {
            Criterion criterion1 = Restrictions.eq("orgCode", orgCode);
            Criterion criterion2= Restrictions.ne("status", "1");
            List<SysOrg> sysOrgList=sysOrgDao.find(criterion1,criterion2);
            if(sysOrgList.size() > 0) {
                return 2;
            }
        }
        return 0;
    }

    @Override
    public void switchStatus(String id) {
        if (StringUtils.isNoneBlank(id)){
            SysOrg sysOrg = get(id);
            if(sysOrg.getStatus().equals("0")){
                sysOrg.setStatus("2");
            }else if(sysOrg.getStatus().equals("2")){
                sysOrg.setStatus("0");
            }
            save(sysOrg);
        }
    }

    @Override
    public SysOrg getOrgByCode(String orgCode) {
        Criterion criterion = Restrictions.eq("orgCode", orgCode);
        List<SysOrg> list = super.findList(criterion);
        if (list.size() > 0)
            return list.get(0);
        return null;
    }
}
