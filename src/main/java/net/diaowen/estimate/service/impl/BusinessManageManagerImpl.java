package net.diaowen.estimate.service.impl;

import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.service.BaseServiceImpl;
import net.diaowen.estimate.dao.BusinessManageDao;
import net.diaowen.estimate.entity.BusinessManage;
import net.diaowen.estimate.service.BusinessManageManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service("businessManageManager")
public class BusinessManageManagerImpl extends BaseServiceImpl<BusinessManage, String> implements BusinessManageManager {

    @Autowired
    private BusinessManageDao businessManageDao;

    @Autowired
    private AccountManager accountManager;
    @Override
    public void setBaseDao() {
        this.baseDao = businessManageDao;
    }


    @Override
    public Page<BusinessManage> findPage(Page<BusinessManage> page, BusinessManage entity) {

        List<Criterion> criterions=new ArrayList<Criterion>();
        Integer status = entity.getDataStatus();
        String businessName = entity.getBusinessName();
        if(status!=null && !"".equals(status)){
            criterions.add(Restrictions.eq("dataStatus", status));
        }
        else
        {
            criterions.add(Restrictions.ne("dataStatus",1));
        }
        if(businessName!=null && !"".equals(businessName)){
            criterions.add(Restrictions.like("businessName", "%"+businessName+"%"));
        }

        //criterions.add(Restrictions.disjunction().add(Restrictions.eq("visibility", 1)).add(Restrictions.isNull("visibility")));

        page.setOrderBy("createTime");
        page.setOrderDir("desc");
        return super.findPageByCri(page, criterions);
    }

    @Override
    public List<BusinessManage> getNvMenuList() {
        List<BusinessManage> listResult = new ArrayList<BusinessManage>();

        Criterion criterion = Restrictions.eq("dataStatus", 0);
        listResult = super.findList(criterion);
        listResult = listResult.stream().sorted(Comparator.comparing(BusinessManage::getId,
                Comparator.nullsFirst(String::compareTo)))
                .collect(Collectors.toList());

        return listResult;
    }

    @Transactional
    @Override
    public BusinessManage SaveBusinessManage(BusinessManage entity) {
        User user = accountManager.getCurUser();
        if(user!=null) {
            if (entity != null) {
                if (StringUtils.isNotEmpty(entity.getId())) {
                    entity.setId(null);
                }
                entity.setUserIdCreate(user.getLoginName());
                //entity.setDataStatus(0);
                Timestamp timestamp = new Timestamp((new Date().getTime()));
                entity.setCreateTime(timestamp);

                super.save(entity);
                return entity;
            }
        }
        return null;
    }

    @Transactional
    @Override
    public BusinessManage UpdateBusinessManage(BusinessManage entity) {
        User user = accountManager.getCurUser();
        if(user!=null){
            if(entity!=null) {
                String id = entity.getId();
                if (id != null) {
                    BusinessManage dbBusinessManage = getModel(id);
                    dbBusinessManage.setBusinessName(entity.getBusinessName());
                    dbBusinessManage.setDataStatus(entity.getDataStatus());
                    dbBusinessManage.setUserIdModified(user.getLoginName());
                    Timestamp timestamp = new Timestamp((new Date().getTime()));
                    dbBusinessManage.setModifiedTime(timestamp);
                    super.save(dbBusinessManage);
                    return dbBusinessManage;
                }
            }
        }
        return null;
    }

    @Override
    public boolean checkBusinessManageExists(BusinessManage entity) {
        boolean isExists = false;
        isExists =  businessManageDao.checkBusinessManageExists(entity);

        return isExists;
    }

    @Transactional
    @Override
    public void deleteData(String[] ids) {
        for (String id:ids) {
            businessManageDao.delete(id);
        }
    }

    @Transactional
    @Override
    public void disableData(String[] ids) {
        User user = accountManager.getCurUser();
        if(user!=null) {
            for (String id : ids) {
                BusinessManage dbBusinessManage = getModel(id);
                dbBusinessManage.setDataStatus(1);
                dbBusinessManage.setUserIdModified(user.getLoginName());
                Timestamp timestamp = new Timestamp((new Date().getTime()));
                dbBusinessManage.setModifiedTime(timestamp);
                super.save(dbBusinessManage);
            }
        }
    }


}
