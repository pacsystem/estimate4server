package net.diaowen.estimate.service.impl;

import net.diaowen.common.base.entity.User;
import net.diaowen.common.base.service.AccountManager;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.service.BaseServiceImpl;
import net.diaowen.estimate.dao.CommonDataDictDao;
import net.diaowen.estimate.entity.CommonDataDict;
import net.diaowen.estimate.service.CommonDataDictManager;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 导入错误记录题
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
@Service
public class CommonDataDictManagerImpl extends BaseServiceImpl<CommonDataDict, String> implements CommonDataDictManager {
    @Autowired
    private CommonDataDictDao commonDataDictDao;

    @Autowired
    private AccountManager accountManager;

    @Override
    public Page<CommonDataDict> findPage(Page<CommonDataDict> page, CommonDataDict entity) {
        List<Criterion> criterions=new ArrayList<Criterion>();
        Integer dataStatus = entity.getDataStatus();
        String itemName = entity.getItemName();
        String itemCode = entity.getItemCode();
        String dictParentId = entity.getDictParentId();

        if(dataStatus!=null && !"".equals(dataStatus)){
            criterions.add(Restrictions.eq("dataStatus", dataStatus));
        }
        else
        {
            //criterions.add(Restrictions.ne("dataStatus",1));
        }

        if(itemName!=null && !"".equals(itemName)){
            criterions.add(Restrictions.like("itemName", "%"+itemName+"%"));
        }

        if(itemCode!=null && !"".equals(itemCode)){
            criterions.add(Restrictions.eq("itemCode", itemCode));
        }

        if(dictParentId!=null && !"".equals(dictParentId)){
            criterions.add(Restrictions.eq("dictParentId", dictParentId));
        }

        //criterions.add(Restrictions.disjunction().add(Restrictions.eq("visibility", 1)).add(Restrictions.isNull("visibility")));

//        page.setOrderBy("createTime");
//        page.setOrderDir("desc");
        page.setOrderBy("dictOrder,createTime");
        page.setOrderDir("asc,desc");
        return super.findPageByCri(page, criterions);
    }

    @Override
    public List<CommonDataDict> getDataDictItemByCode(String itemCode) {
        return commonDataDictDao.getDataDictItemByCode(itemCode);
    }

    @Transactional
    @Override
    public CommonDataDict saveCommonDataDict(CommonDataDict entity) {
        User user = accountManager.getCurUser();
        if(user!=null) {
            if (entity != null) {
                if (StringUtils.isNotEmpty(entity.getId())) {
                    entity.setId(null);
                }
                entity.setCreator(user.getLoginName());
                //entity.setDataStatus(0);
                Timestamp timestamp = new Timestamp((new Date().getTime()));
                entity.setCreateTime(timestamp);

                commonDataDictDao.save(entity);

                return entity;
            }
        }
        return null;
    }

    @Transactional
    @Override
    public CommonDataDict updateCommonDataDict(CommonDataDict entity) {
        User user = accountManager.getCurUser();
        if(user!=null){
            if(entity!=null) {
                String id = entity.getId();
                if (id != null) {
                    CommonDataDict commonDataDict = super.findById(id);// getModel(id);
                    if(commonDataDict!=null){
                        List<CommonDataDict> mlist = commonDataDictDao.getDataDictItemByCode(commonDataDict.getItemCode());
                        BeanUtils.copyProperties(entity, commonDataDict);
                        commonDataDict.setDataStatus(entity.getDataStatus());
                        commonDataDict.setModifier(user.getLoginName());
                        Timestamp timestamp = new Timestamp((new Date().getTime()));
                        commonDataDict.setModifyTime(timestamp);
                        super.save(commonDataDict);
                        if((mlist!=null) && (mlist.size()>0)) {
                            List<CommonDataDict> list = mlist.get(0).getChildren();
                            if ((list != null) && (list.size() == 1)) {
                                //List<CommonDataDict> subList = list.get(0).getChildren();
                                for (int i = 0; i < list.size(); i++) {
                                    CommonDataDict temp = list.get(i);
                                    temp.setItemCode(entity.getItemCode() + ":" + temp.getItemValue());
                                    super.save(temp);
                                }
                            }
                        }
                    }
                    return commonDataDict;
                }
            }
        }
        return null;
    }

    @Override
    public boolean checkCommonDataDictExists(CommonDataDict entity) {
        boolean isExists = false;
        isExists =  commonDataDictDao.checkCommonDataDictExists(entity);

        return isExists;
    }

    @Transactional
    @Override
    public void deleteData(String[] ids) {
        for (String id:ids) {
            commonDataDictDao.delete(id);
        }
    }

    @Transactional
    @Override
    public void disableData(String[] ids) {
        User user = accountManager.getCurUser();
        if(user!=null) {
            for (String id : ids) {
                CommonDataDict dbCommonDataDict = getModel(id);
                dbCommonDataDict.setDataStatus(1);
                dbCommonDataDict.setModifier(user.getLoginName());
                Timestamp timestamp = new Timestamp((new Date().getTime()));
                dbCommonDataDict.setModifyTime(timestamp);
                super.save(dbCommonDataDict);
            }
        }
    }

    @Override
    public CommonDataDict getCommonDataDictById(String id) {
        return super.findById(id);
    }

    @Override
    public void setBaseDao() {
        this.baseDao = commonDataDictDao;
    }
}
