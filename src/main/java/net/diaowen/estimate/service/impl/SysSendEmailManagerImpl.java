package net.diaowen.estimate.service.impl;

import net.diaowen.estimate.dao.SysSendEmailDao;
import net.diaowen.estimate.entity.SysSendEmail;
import net.diaowen.estimate.service.SysSendEmailManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.diaowen.common.service.BaseServiceImpl;

@Service
public class SysSendEmailManagerImpl extends BaseServiceImpl<SysSendEmail, String> implements SysSendEmailManager {

	@Autowired
	private SysSendEmailDao sysSendEmailDao;

	@Override
	public void setBaseDao() {
		this.baseDao=sysSendEmailDao;
	}

	@Override
	public void scanning() {
		//扫描没有发送的邮件，进行邮件发送操作，ok.

	}

}
