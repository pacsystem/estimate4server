package net.diaowen.estimate.service.impl;

import net.diaowen.estimate.dao.SysEmailDao;
import net.diaowen.estimate.entity.SysEmail;
import net.diaowen.estimate.service.SysEmailManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.diaowen.common.service.BaseServiceImpl;

@Service
public class SysEmailManagerImpl extends BaseServiceImpl<SysEmail, String> implements SysEmailManager {

	@Autowired
	private SysEmailDao sysEmailDao;

	@Override
	public void setBaseDao() {
		this.baseDao=sysEmailDao;
	}

}
