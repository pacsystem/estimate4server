package net.diaowen.estimate.service.impl;

import net.diaowen.estimate.service.SysDbBackupManager;
import net.diaowen.estimate.dao.SysDbBackupDao;
import net.diaowen.estimate.entity.SysDbBackup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.diaowen.common.service.BaseServiceImpl;


/**
 * 问卷备份
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
@Service
public class SysDbBackupManagerImpl extends BaseServiceImpl<SysDbBackup, String> implements SysDbBackupManager {

	@Autowired
	private SysDbBackupDao sysDbBackupDao;

	@Override
	public void setBaseDao() {
		this.baseDao=sysDbBackupDao;
	}

}
