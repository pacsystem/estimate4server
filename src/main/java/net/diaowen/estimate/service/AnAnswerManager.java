package net.diaowen.estimate.service;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.AnAnswer;

/**
 * 答卷业务
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

public interface AnAnswerManager extends BaseService<AnAnswer, String> {
	public AnAnswer findAnswer(String belongAnswerId, String quId);

	public void findGroupStats(Question question);
}
