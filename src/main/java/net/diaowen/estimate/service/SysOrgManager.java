package net.diaowen.estimate.service;

import net.diaowen.common.base.entity.User;
import net.diaowen.common.plugs.httpclient.HttpResult;
import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.SysOrg;

import java.util.List;
import java.util.Map;

public interface SysOrgManager extends BaseService<SysOrg, String> {

    public Page<SysOrg> findPage(Page<SysOrg> page, String orgName);

    public SysOrg orgCreate(SysOrg entity);

    public HttpResult orgUpdate(SysOrg entity);

    public void orgDelete(String[] ids);

    public Integer check(String orgName, String orgCode);

    public void switchStatus(String id);

    public SysOrg getOrgByCode(String orgCode);
}
