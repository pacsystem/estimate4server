package net.diaowen.estimate.service;

import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.AnUplodFile;
import net.diaowen.estimate.entity.Question;

import java.util.List;

/**
 * 填空题
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
public interface AnUploadFileManager extends BaseService<AnUplodFile, String> {

	public List<AnUplodFile> findAnswer(String belongAnswerId, String quId);

	public void findGroupStats(Question question);

	public List<AnUplodFile> findAnswer(String belongAnswerId);

}
