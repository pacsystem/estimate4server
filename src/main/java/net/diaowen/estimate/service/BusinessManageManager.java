package net.diaowen.estimate.service;

import net.diaowen.common.plugs.page.Page;
import net.diaowen.common.service.BaseService;
import net.diaowen.estimate.entity.BusinessManage;

import java.util.List;

public interface BusinessManageManager  extends BaseService<BusinessManage, String> {

    public Page<BusinessManage> findPage(Page<BusinessManage> page, BusinessManage entity);
    public List<BusinessManage> getNvMenuList();
    public BusinessManage SaveBusinessManage(BusinessManage entity);
    public BusinessManage UpdateBusinessManage(BusinessManage entity);
    public boolean checkBusinessManageExists(BusinessManage entity);
    public void deleteData(String[] ids);
    public void disableData(String[] ids);
}
