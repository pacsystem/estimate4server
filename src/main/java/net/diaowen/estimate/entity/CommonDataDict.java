package net.diaowen.estimate.entity;

import net.diaowen.common.base.entity.IdEntity;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "t_common_datadict")
@AttributeOverride(name = "id", column = @Column(name = "dict_id",length = 255))
public class CommonDataDict extends IdEntity {

  private String itemCode;
  private String itemValue;
  private String itemName;
  private String dictParentId;
  private String spellCode;
  private Integer dataStatus;
  private Integer dictOrder;
  private java.sql.Timestamp createTime;
  private String creator;
  private java.sql.Timestamp modifyTime;
  private String modifier;
  private String serviceId;

  @Transient
  public List<CommonDataDict> getChildren() {
    return children;
  }

  public void setChildren(List<CommonDataDict> children) {
    this.children = children;
  }

  private List<CommonDataDict> children = new ArrayList<CommonDataDict>();

//  public String getDictId() {
//    return dictId;
//  }
//
//  public void setDictId(String dictId) {
//    this.dictId = dictId;
//  }


  public String getItemCode() {
    return itemCode;
  }

  public void setItemCode(String itemCode) {
    this.itemCode = itemCode;
  }


  public String getItemValue() {
    return itemValue;
  }

  public void setItemValue(String itemValue) {
    this.itemValue = itemValue;
  }


  public String getItemName() {
    return itemName;
  }

  public void setItemName(String itemName) {
    this.itemName = itemName;
  }


  public String getDictParentId() {
    return dictParentId;
  }

  public void setDictParentId(String dictParentId) {
    this.dictParentId = dictParentId;
  }


  public String getSpellCode() {
    return spellCode;
  }

  public void setSpellCode(String spellCode) {
    this.spellCode = spellCode;
  }


  public Integer getDataStatus() {
    return dataStatus;
  }

  public void setDataStatus(Integer dataStatus) {
    this.dataStatus = dataStatus;
  }


  public Integer getDictOrder() {
    return dictOrder;
  }

  public void setDictOrder(Integer dictOrder) {
    this.dictOrder = dictOrder;
  }


  public java.sql.Timestamp getCreateTime() {
    return createTime;
  }

  public void setCreateTime(java.sql.Timestamp createTime) {
    this.createTime = createTime;
  }


  public String getCreator() {
    return creator;
  }

  public void setCreator(String creator) {
    this.creator = creator;
  }


  public java.sql.Timestamp getModifyTime() {
    return modifyTime;
  }

  public void setModifyTime(java.sql.Timestamp modifyTime) {
    this.modifyTime = modifyTime;
  }


  public String getModifier() {
    return modifier;
  }

  public void setModifier(String modifier) {
    this.modifier = modifier;
  }


  public String getServiceId() {
    return serviceId;
  }

  public void setServiceId(String serviceId) {
    this.serviceId = serviceId;
  }

}
