package net.diaowen.estimate.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import net.diaowen.common.base.entity.IdEntity;

/**
 * 具体的一次调查
 *
 * @author keyuan
 * <p>
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */
@Entity
@Table(name = "t_survey_answer")
public class SurveyAnswer extends IdEntity {
    //问卷ID
    private String surveyId;
    //回答者ID
    private String userId;
    //回答时间
    private Date bgAnDate;
    //回答时间
    private Date endAnDate = new Date();
    //用时
    private Float totalTime;
    //回答者IP
    private String ipAddr;
    //回答者是详细地址
    private String addr;
    //回答者城市
    private String city;
    //回答者MAC
    private String pcMac;
    //回答的题数
    private Integer quNum;

    /** 回复的此条数据统计信息 **/

    /**
     * 数据--完成情况    是否全部题都回答
     **/
    //是否完成  1完成
    private Integer isComplete;
    //回答的题数
    private Integer completeNum;
    //回答的题项目数 ---- 表示有些题下面会有多重回答
    private Integer completeItemNum;

    /**
     * 数据--有效情况   根据设计问卷时指定的必填项
     **/
    //是否是有效数据  1有效
    private Integer isEffective;

    /**
     * 数据--审核情况
     **/
    //审核状态  0未处理 1通过 2不通过
    private Integer handleState = 0;

    /**
     * 不同来源数据
     **/
    //数据来源  0网调  1录入数据 2移动数据 3导入数据
    private Integer dataSource = 0;

    //问卷ID
    @Transient
    private String surveyName;
    /**
     * 评价人姓名
     */
    @Column(name = "estimate_name")
    private String estimateName;

    /**
     * 评价人性别
     */
    @Column(name = "estimate_gender")
    private String estimateGender;
    /**
     * 评价人年龄
     */
    @Column(name = "estimate_age")
    private String estimateAge;

    /**
     * 门诊号
     */
    @Column(name = "outpatient_number")
    private String outpatientNumber;

    /**
     * 住院号
     */
    @Column(name = "hospital_number")
    private String hospitalNumber;


    /**
     * 检查号
     */
    @Column(name = "inspect_number")
    private String inspectNumber;

    /**
     * 来源
     */
    @Column(name = "source")
    private String source;

    /**
     * 来源编号
     */
    private String sourceNumber;


    @Column(name = "business_type_id")
    private String businessTypeId;


    /**
     * 机构名称
     */
    @Column(name = "org_name")
    private String orgName;

    /**
     * 机构名称
     */
    @Column(name = "org_code")
    private String orgCode;

    /**
     * 评价模板
     */
    @Column(name = "estimate_template_name")
    private String estimateTemplateName;

    /**
     * 评价时间
     */
    @Column(name = "estimate_date")
    private Date estimateDate;

    /**
     * 评价人员
     */
    @Column(name = "estimate_person")
    private String estimatePerson;

    /**
     * 评分
     */
    @Column(name = "estimate_score")
    private String estimateScore;


    /**
     * 评分
     */
    @Column(name = "estimate_status")
    private String estimateStatus;

    /**
     * 评分
     */
    @Column(name = "answer_id")
    private String answerId;

    @Column(name = "business_name")
    private String businessName;

    @Column(name = "business_order_code")
    private String businessOrderCode;

    // 评价维度
    private String appraise;

    private String appraiseName;

    public String getEstimateTemplateName() {
        return estimateTemplateName;
    }

    public void setEstimateTemplateName(String estimateTemplateName) {
        this.estimateTemplateName = estimateTemplateName;
    }

    public String getSurveyName() {
        return surveyName;
    }

    public void setSurveyName(String surveyName) {
        this.surveyName = surveyName;
    }

    public String getOrgCode() {
        return orgCode;
    }

    public void setOrgCode(String orgCode) {
        this.orgCode = orgCode;
    }

    public Date getEstimateDate() {
        return estimateDate;
    }

    public void setEstimateDate(Date estimateDate) {
        this.estimateDate = estimateDate;
    }

    public String getEstimatePerson() {
        return estimatePerson;
    }

    public void setEstimatePerson(String estimatePerson) {
        this.estimatePerson = estimatePerson;
    }

    public String getEstimateScore() {
        return estimateScore;
    }

    public void setEstimateScore(String estimateScore) {
        this.estimateScore = estimateScore;
    }

    public String getEstimateStatus() {
        return estimateStatus;
    }

    public void setEstimateStatus(String estimateStatus) {
        this.estimateStatus = estimateStatus;
    }

    public String getAnswerId() {
        return answerId;
    }

    public void setAnswerId(String answerId) {
        this.answerId = answerId;
    }

    public String getBusinessTypeId() {
        return businessTypeId;
    }

    public void setBusinessTypeId(String businessTypeId) {
        this.businessTypeId = businessTypeId;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }



    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getSourceNumber() {
        return sourceNumber;
    }

    public void setSourceNumber(String sourceNumber) {
        this.sourceNumber = sourceNumber;
    }

    public String getEstimateName() {
        return estimateName;
    }

    public void setEstimateName(String estimateName) {
        this.estimateName = estimateName;
    }

    public String getEstimateGender() {
        return estimateGender;
    }

    public void setEstimateGender(String estimateGender) {
        this.estimateGender = estimateGender;
    }

    public String getEstimateAge() {
        return estimateAge;
    }

    public void setEstimateAge(String estimateAge) {
        this.estimateAge = estimateAge;
    }

    public String getOutpatientNumber() {
        return outpatientNumber;
    }

    public void setOutpatientNumber(String outpatientNumber) {
        this.outpatientNumber = outpatientNumber;
    }

    public String getHospitalNumber() {
        return hospitalNumber;
    }

    public void setHospitalNumber(String hospitalNumber) {
        this.hospitalNumber = hospitalNumber;
    }

    public String getInspectNumber() {
        return inspectNumber;
    }

    public void setInspectNumber(String inspectNumber) {
        this.inspectNumber = inspectNumber;
    }

    public String getSurveyId() {
        return surveyId;
    }

    public void setSurveyId(String surveyId) {
        this.surveyId = surveyId;
    }

    public Date getBgAnDate() {
        return bgAnDate;
    }

    public void setBgAnDate(Date bgAnDate) {
        this.bgAnDate = bgAnDate;
    }

    public Date getEndAnDate() {
        return endAnDate;
    }

    public void setEndAnDate(Date endAnDate) {
        this.endAnDate = endAnDate;
    }

    public String getIpAddr() {
        return ipAddr;
    }

    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }

    public Integer getQuNum() {
        return quNum;
    }

    public void setQuNum(Integer quNum) {
        this.quNum = quNum;
    }

    public Float getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Float totalTime) {
        this.totalTime = totalTime;
    }

    public String getPcMac() {
        return pcMac;
    }

    public void setPcMac(String pcMac) {
        this.pcMac = pcMac;
    }

    public String getAddr() {
        return addr;
    }

    public void setAddr(String addr) {
        this.addr = addr;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getIsComplete() {
        return isComplete;
    }

    public void setIsComplete(Integer isComplete) {
        this.isComplete = isComplete;
    }

    public Integer getCompleteNum() {
        return completeNum;
    }

    public void setCompleteNum(Integer completeNum) {
        this.completeNum = completeNum;
    }

    public Integer getIsEffective() {
        return isEffective;
    }

    public void setIsEffective(Integer isEffective) {
        this.isEffective = isEffective;
    }

    public Integer getHandleState() {
        return handleState;
    }

    public void setHandleState(Integer handleState) {
        this.handleState = handleState;
    }

    public Integer getDataSource() {
        return dataSource;
    }

    public void setDataSource(Integer dataSource) {
        this.dataSource = dataSource;
    }

    public Integer getCompleteItemNum() {
        return completeItemNum;
    }

    public void setCompleteItemNum(Integer completeItemNum) {
        this.completeItemNum = completeItemNum;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getBusinessOrderCode() {
        return businessOrderCode;
    }

    public void setBusinessOrderCode(String businessOrderCode) {
        this.businessOrderCode = businessOrderCode;
    }


    private SurveyDirectory surveyDirectory;

    @Transient
    public SurveyDirectory getSurveyDirectory() {
        return surveyDirectory;
    }

    public void setSurveyDirectory(SurveyDirectory surveyDirectory) {
        this.surveyDirectory = surveyDirectory;
    }

    public String getAppraise() {
        return appraise;
    }
    public void setAppraise(String appraise) {
        this.appraise = appraise;
    }

    @Transient
    public String getAppraiseName() {
        return appraiseName;
    }
    public void setAppraiseName(String appraiseName) {
        this.appraiseName = appraiseName;
    }

}
