package net.diaowen.estimate.entity;

import net.diaowen.common.base.entity.IdEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;
import java.util.Date;

/**
 * 业务系统表实例
 * @author dragon2snow
 * @date 2022年7月1日
 *
 * https://
 */
@Entity
@Table(name = "t_business_manage")
public class BusinessManage extends IdEntity {




    /**
     * 备注
     */
    @Column(name = "business_name")
    private String businessName;
    /**
     * 备注
     */
    @Column(name = "remarks")
    private String remarks;

    /**
     * 数据状态 (0 正常 1 删除 2 停用）
     */
    @Column(name = "data_status")
    private Integer dataStatus;

    /**
     * 创建人
     */
    @Column(name = "user_id_create")
    private String userIdCreate;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    private Timestamp createTime;

    /**
     * 修改人
     */
    @Column(name = "user_id_modified")
    private String userIdModified;

    /**
     * 修改时间
     */
    @Column(name = "modified_time")
    private Timestamp modifiedTime;

    public String getBusinessName() {
        return businessName;
    }

    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    public Integer getDataStatus() {
        return dataStatus;
    }

    public void setDataStatus(Integer dataStatus) {
        this.dataStatus = dataStatus;
    }

    public String getUserIdCreate() {
        return userIdCreate;
    }

    public void setUserIdCreate(String userIdCreate) {
        this.userIdCreate = userIdCreate;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public String getUserIdModified() {
        return userIdModified;
    }

    public void setUserIdModified(String userIdModified) {
        this.userIdModified = userIdModified;
    }

    public Date getModifiedTime() {
        return modifiedTime;
    }

    public void setModifiedTime(Timestamp modifiedTime) {
        this.modifiedTime = modifiedTime;
    }
}
