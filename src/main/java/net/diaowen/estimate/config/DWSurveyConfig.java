package net.diaowen.estimate.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class DWSurveyConfig {

    public static String DWSURVEY_SITE = null;
    public static String DWSURVEY_WEB_FILE_PATH = null;
    public static String DWSURVEY_WEB_SITE_URL = null;
    public static String DWSURVEY_WEB_RESOURCE_URL = null;
    public static String DWSURVEY_WEB_STATIC_TYPE = null;
    public static String DWSURVEY_WEIXIN_APP_ID = null;
    public static String DWSURVEY_WEIXIN_APP_SECRET = null;
    public static String DWSURVEY_WEIXIN_SERVER_TOKEN = null;
    public static String DWSURVEY_WEIXIN_SERVER_AESKEY = null;

    public static String DWSURVEY_VERSION_INFO = null;
    public static String DWSURVEY_VERSION_NUMBER = null;
    public static String DWSURVEY_VERSION_BUILT = null;
    public static String DWSURVEY_WEB_INFO_SITE_NAME = null;
    public static String DWSURVEY_WEB_INFO_SITE_URL = null;
    public static String DWSURVEY_WEB_INFO_SITE_ICP = null;
    public static String DWSURVEY_WEB_INFO_SITE_MAIL = null;
    public static String DWSURVEY_WEB_INFO_SITE_PHONE = null;


    @Value("${estimate.web.file-path}")
    public void setWebFilePath(String webFilePath) {
        DWSurveyConfig.DWSURVEY_WEB_FILE_PATH = webFilePath;
    }

    @Value("${estimate.web.site-url}")
    public void setWebSiteUrl(String webSiteUrl) {
        DWSurveyConfig.DWSURVEY_WEB_SITE_URL = webSiteUrl;
    }

    @Value("${estimate.web.resource-url}")
    public void setWebResourceUrl(String dwsurveyWebResourceUrl) {
        DWSURVEY_WEB_RESOURCE_URL = dwsurveyWebResourceUrl;
    }

    @Value("${estimate.web.static-type}")
    public void setWebStaticType(String webStaticType) {
        DWSurveyConfig.DWSURVEY_WEB_STATIC_TYPE = webStaticType;
    }

    @Value("${estimate.weixin.app-id}")
    public void setWeixinAppId(String weixinAppId) {
        DWSurveyConfig.DWSURVEY_WEIXIN_APP_ID = weixinAppId;
    }

    @Value("${estimate.weixin.app-secret}")
    public void setWeixinAppSecret(String weixinAppSecret) {
        DWSurveyConfig.DWSURVEY_WEIXIN_APP_SECRET = weixinAppSecret;
    }

    @Value("${estimate.weixin.server.token}")
    public void setDwsurveyWeixinServerToken(String weixinServerToken) {
        DWSurveyConfig.DWSURVEY_WEIXIN_SERVER_TOKEN = weixinServerToken;
    }

    @Value("${estimate.weixin.server.encodingAESKey}")
    public void setDwsurveyWeixinServerAeskey(String weixinAppSecret) {
        DWSurveyConfig.DWSURVEY_WEIXIN_SERVER_AESKEY = weixinAppSecret;
    }

    @Value("${estimate.site}")
    public void setDwsurveyDemo(String dwsurveySite) {
        DWSurveyConfig.DWSURVEY_SITE = dwsurveySite;
    }


    @Value("${estimate.version.info}")
    public void setDwsurveyVersionInfo(String dwsurveyVersionInfo) {
        DWSURVEY_VERSION_INFO = dwsurveyVersionInfo;
    }

    @Value("${estimate.version.number}")
    public void setDwsurveyVersionNumber(String dwsurveyVersionNumber) { DWSURVEY_VERSION_NUMBER = dwsurveyVersionNumber; }

    @Value("${estimate.version.built}")
    public void setDwsurveyVersionBuilt(String dwsurveyVersionBuilt) {
        DWSURVEY_VERSION_BUILT = dwsurveyVersionBuilt;
    }

    @Value("${estimate.web.info.site-name}")
    public void setDwsurveyWebInfoName(String dwsurveyWebInfoName) { DWSURVEY_WEB_INFO_SITE_NAME = dwsurveyWebInfoName;}

    @Value("${estimate.web.info.site-url}")
    public void setDwsurveyWebInfoUrl(String dwsurveyWebInfoName) {
        DWSURVEY_WEB_INFO_SITE_URL = dwsurveyWebInfoName;
    }

    @Value("${estimate.web.info.site-icp}")
    public void setDwsurveyWebInfoIcp(String dwsurveyWebInfoIcp) {
        DWSURVEY_WEB_INFO_SITE_ICP = dwsurveyWebInfoIcp;
    }

    @Value("${estimate.web.info.site-mail}")
    public void setDwsurveyWebInfoMail(String dwsurveyWebInfoMail) { DWSURVEY_WEB_INFO_SITE_MAIL = dwsurveyWebInfoMail; }

    @Value("${estimate.web.info.site-phone}")
    public void setDwsurveyWebInfoSitePhone(String dwsurveyWebInfoSitePhone) { DWSURVEY_WEB_INFO_SITE_PHONE = dwsurveyWebInfoSitePhone; }
}
