package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SurveyDetail;

public interface SurveyDetailDao extends BaseDao<SurveyDetail, String>{

}
