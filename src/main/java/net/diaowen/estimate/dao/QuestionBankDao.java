package net.diaowen.estimate.dao;


import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.QuestionBank;

/**
 * 题库 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface QuestionBankDao extends BaseDao<QuestionBank, String>{

}
