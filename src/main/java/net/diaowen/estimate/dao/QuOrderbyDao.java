package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.QuOrderby;

public interface QuOrderbyDao extends BaseDao<QuOrderby, String> {

}
