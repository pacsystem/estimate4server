package net.diaowen.estimate.dao;

import java.util.List;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.AnCheckbox;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.DataCross;


/**
 * 多选题数据 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface AnCheckboxDao extends BaseDao<AnCheckbox, String>{

	public void findGroupStats(Question question);

	public List<DataCross> findStatsDataCross(Question rowQuestion,
                                              Question colQuestion);

	public List<DataCross> findStatsDataChart(Question question);

}
