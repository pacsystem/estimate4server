package net.diaowen.estimate.dao;

import java.util.Map;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SurveyStats;
import net.diaowen.estimate.entity.SurveyAnswer;

public interface SurveyAnswerDao extends BaseDao<SurveyAnswer, String>{

	public void saveAnswer(SurveyAnswer surveyAnswer,
                           Map<String, Map<String, Object>> quMaps);

	public SurveyStats surveyStatsData(SurveyStats surveyStats);

    Long countResult(String id);

	public SurveyAnswer findBysourceNumber(String SourceNumber);


	public void upanswer_estimatescore(String qu_id,String belong_answer_id);

	public void updateAnswerAnResult(String belong_answer_id);
}
