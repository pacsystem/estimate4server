package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SysDbBackup;


public interface SysDbBackupDao extends BaseDao<SysDbBackup, String>{

}
