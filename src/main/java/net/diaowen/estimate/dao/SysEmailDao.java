package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SysEmail;

public interface SysEmailDao extends BaseDao<SysEmail, String>{

}
