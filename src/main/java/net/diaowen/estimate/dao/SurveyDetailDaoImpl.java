package net.diaowen.estimate.dao;

import net.diaowen.estimate.entity.SurveyDetail;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

@Repository
public class SurveyDetailDaoImpl extends BaseDaoImpl<SurveyDetail, String> implements SurveyDetailDao {

}
