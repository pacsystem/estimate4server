package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.AnAnswer;
import net.diaowen.estimate.entity.Question;

/**
 * 答卷数据 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface AnAnswerDao extends BaseDao<AnAnswer, String> {

	public void findGroupStats(Question question);

}
