package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SysSendEmail;

public interface SysSendEmailDao extends BaseDao<SysSendEmail, String>{

}
