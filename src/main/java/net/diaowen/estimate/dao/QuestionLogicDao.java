package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.QuestionLogic;

/**
 * 题逻辑 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface QuestionLogicDao extends BaseDao<QuestionLogic, String> {

}
