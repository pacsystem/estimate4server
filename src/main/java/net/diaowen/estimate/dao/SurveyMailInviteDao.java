package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SurveyMailInvite;

public interface SurveyMailInviteDao extends BaseDao<SurveyMailInvite, String>{

}
