package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SurveyDirectory;

public interface SurveyDirectoryDao extends BaseDao<SurveyDirectory, String>{

    public boolean checkSurveyDirectoryExists(SurveyDirectory surveyDirectory);


    public boolean upDateSurveyDirectory(SurveyDirectory surveyDirectory);
}
