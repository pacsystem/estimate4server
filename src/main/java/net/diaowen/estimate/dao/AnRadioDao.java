package net.diaowen.estimate.dao;

import java.util.List;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.AnRadio;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.DataCross;

/**
 * 单选题 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface AnRadioDao extends BaseDao<AnRadio, String> {

	public void findGroupStats(Question question);

	public List<DataCross> findStatsDataCross(Question rowQuestion,
                                              Question colQuestion);

	public List<DataCross> findStatsDataChart(Question question);

}
