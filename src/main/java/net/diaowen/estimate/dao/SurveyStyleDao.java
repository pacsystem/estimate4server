package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SurveyStyle;

public interface SurveyStyleDao extends BaseDao<SurveyStyle, String>{

}
