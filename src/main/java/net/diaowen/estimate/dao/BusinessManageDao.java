package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.BusinessManage;

/**
 * 业务系统表DAO接口
 * @author dragon2snow
 * @date 2022年7月1日
 *
 * https://
 */
public interface BusinessManageDao extends BaseDao<BusinessManage, String> {
    public boolean checkBusinessManageExists(BusinessManage businessManage);
}
