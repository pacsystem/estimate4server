package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.Question;
import net.diaowen.estimate.entity.SurveyStats;

public interface SurveyStatsDao extends BaseDao<SurveyStats, String>{

	public void findStatsDataCross(Question rowQuestion, Question colQuestion);

}
