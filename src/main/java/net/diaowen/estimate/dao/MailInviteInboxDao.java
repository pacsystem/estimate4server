package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.MailInviteInbox;

public interface MailInviteInboxDao extends BaseDao<MailInviteInbox, String>{

}
