package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.AnUplodFile;
import net.diaowen.estimate.entity.Question;

/**
 * 填空题 interface
 * @author KeYuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 *
 */
public interface AnUploadFileDao extends BaseDao<AnUplodFile, String> {

	public void findGroupStats(Question question);

}
