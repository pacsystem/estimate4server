package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.BusinessManage;
import net.diaowen.estimate.entity.CommonDataDict;

import java.util.List;

public interface CommonDataDictDao extends BaseDao<CommonDataDict, String> {
   List<CommonDataDict> getDataDictItemByCode(String itemCode);
   CommonDataDict getOneDataDictItemByDictID(String dictId);
   public boolean checkCommonDataDictExists(CommonDataDict commonDataDict);
}
