package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.QuMultiFillblankDao;
import net.diaowen.estimate.entity.QuMultiFillblank;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

/**
 * 多行填空题 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

//@Repository("quMultiFillblankDao")
@Repository
public class QuMultiFillblankDaoImpl extends BaseDaoImpl<QuMultiFillblank, String> implements QuMultiFillblankDao {

}
