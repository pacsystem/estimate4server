package net.diaowen.estimate.dao.impl;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.CommonDataDictDao;
import net.diaowen.estimate.entity.CommonDataDict;
import org.apache.commons.lang.StringUtils;
import org.hibernate.Session;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.*;

@Repository
public class CommonDataDictDaoImpl extends BaseDaoImpl<CommonDataDict, String> implements CommonDataDictDao {

    @Override
    public List<CommonDataDict> getDataDictItemByCode(String itemCode) {
        Session session=  this.getSession();
        CriteriaBuilder builder = session.getCriteriaBuilder();
        CriteriaQuery<CommonDataDict> criteria = builder.createQuery(CommonDataDict.class);
        Root<CommonDataDict> root = criteria.from(CommonDataDict.class);
        criteria.select(root);
        List<Predicate> predicateList = new ArrayList<>();
        predicateList.add(builder.notEqual(root.get("dataStatus"),1));
        predicateList.add(builder.equal(root.get("itemCode"),itemCode));

        Predicate[] predicates = predicateList.toArray(new Predicate[0]);
        criteria.where(predicates);
        criteria.orderBy(builder.desc(root.get("dictOrder")));

        List<CommonDataDict> list = session.createQuery(criteria).list();
        for (int i = 0; i < list.size(); i++) {
           String dictId =  list.get(i).getId();
            CriteriaQuery<CommonDataDict> subCriteria = builder.createQuery(CommonDataDict.class);
            Root<CommonDataDict> subRoot = subCriteria.from(CommonDataDict.class);
            subCriteria.select(subRoot);
            List<Predicate> subPredicateList = new ArrayList<>();
            subPredicateList.add(builder.notEqual(subRoot.get("dataStatus"),1));
            subPredicateList.add(builder.equal(subRoot.get("dictParentId"),dictId));

            Predicate[] subPredicates = subPredicateList.toArray(new Predicate[0]);
            subCriteria.where(subPredicates);
            subCriteria.orderBy(builder.desc(root.get("dictOrder")));
            List<CommonDataDict> subList = session.createQuery(subCriteria).list();
            if(subList!=null)
            {
                list.get(i).setChildren(subList);
            }
        }

        return list;
    }

    @Override
    public CommonDataDict getOneDataDictItemByDictID(String dictId) {

       return null;
    }

    @Override
    public boolean checkCommonDataDictExists(CommonDataDict commonDataDict) {
        Integer count = 0;
        String id = commonDataDict.getId();
        if (StringUtils.isEmpty(id)) {
            String sql = "select * from t_common_datadict t where t.item_code = ? and t.dict_parent_id = ? and t.data_status != 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql).setParameter(1, commonDataDict.getItemCode())
                    .setParameter(2,commonDataDict.getDictParentId()).list();
            if(list!=null)
                count = list.size();
        } else {
            String sql = "select * from t_common_datadict t where t.item_code = ? and t.dict_parent_id = ? and t.dict_id != ? and t.data_status != 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql).setParameter(1, commonDataDict.getItemCode())
                    .setParameter(2,commonDataDict.getDictParentId())
                    .setParameter(3,commonDataDict.getId()).list();
            if(list!=null)
                count = list.size();
        }

        if (count > 0) {
            return true;
        }

        return false;
    }
}
