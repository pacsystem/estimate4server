package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.SurveyDirectoryDao;
import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.entity.SurveyDirectory;

import java.util.List;

/**
 * 问卷目录 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

//@Repository("surveyDirectoryDao")
@Repository
public class SurveyDirectoryDaoImpl extends BaseDaoImpl<SurveyDirectory, String> implements SurveyDirectoryDao {

    @Override
    public boolean checkSurveyDirectoryExists(SurveyDirectory surveyDirectory) {
        Integer count = 0;
        String id = surveyDirectory.getId();
        if (StringUtils.isEmpty(id)) {
            String sql = "select * from t_survey_directory t where t.survey_name = ? and t.business_type_id = ? and t.visibility = 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql)
                    .setParameter(1, surveyDirectory.getSurveyName())
                    .setParameter(2, surveyDirectory.getBusinessTypeId())
                    .list();
            if(list!=null)
                count = list.size();
        } else {
            String sql = "select * from t_survey_directory t where t.survey_name = ? and t.business_type_id = ? and t.id !=? and t.visibility = 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql)
                    .setParameter(1, surveyDirectory.getSurveyName())
                    .setParameter(2, surveyDirectory.getBusinessTypeId())
                    .setParameter(3, surveyDirectory.getId())
                    .list();
            if(list!=null)
                count = list.size();
        }

        if (count > 0) {
            return true;
        }

        return false;
    }

    @Override
    public boolean upDateSurveyDirectory(SurveyDirectory surveyDirectory) {
         String sql = "update t_survey_directory set survey_name=?,business_type_id=?,appraise=?,business_name=? where id=?";
         SQLQuery query=this.getSession().createSQLQuery(sql);
         query.setParameter(1, surveyDirectory.getSurveyName());
         query.setParameter(2, surveyDirectory.getBusinessTypeId());
         query.setParameter(3, surveyDirectory.getAppraise());
         query.setParameter(4, surveyDirectory.getBusinessName());
         query.setParameter(5, surveyDirectory.getId());
         query.executeUpdate();
         return true;
    }
}
