package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.QuestionBankDao;
import net.diaowen.estimate.entity.QuestionBank;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

/**
 * 题库 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class QuestionBankDaoImpl extends BaseDaoImpl<QuestionBank, String> implements QuestionBankDao {

}
