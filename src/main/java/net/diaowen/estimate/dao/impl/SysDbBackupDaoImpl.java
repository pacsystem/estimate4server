package net.diaowen.estimate.dao.impl;

import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.SysDbBackupDao;
import net.diaowen.estimate.entity.SysDbBackup;

/**
 * 系统备份 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class SysDbBackupDaoImpl extends BaseDaoImpl<SysDbBackup, String> implements SysDbBackupDao{

}
