package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.entity.QuestionLogic;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.QuestionLogicDao;

/**
 * 题逻辑 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class QuestionLogicDaoImpl extends BaseDaoImpl<QuestionLogic, String> implements QuestionLogicDao{

}
