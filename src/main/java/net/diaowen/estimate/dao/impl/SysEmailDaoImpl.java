package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.SysEmailDao;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.entity.SysEmail;

@Repository
public class SysEmailDaoImpl extends BaseDaoImpl<SysEmail, String> implements SysEmailDao {

}
