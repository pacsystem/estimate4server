package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.SurveyMailInviteDao;
import net.diaowen.estimate.entity.SurveyMailInvite;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

@Repository
public class SurveyMailInviteDaoImpl extends BaseDaoImpl<SurveyMailInvite, String> implements SurveyMailInviteDao {

}
