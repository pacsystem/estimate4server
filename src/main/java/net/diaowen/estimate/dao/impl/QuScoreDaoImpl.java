package net.diaowen.estimate.dao.impl;

import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.QuScoreDao;
import net.diaowen.estimate.entity.QuScore;

/**
 * 评分题 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class QuScoreDaoImpl extends BaseDaoImpl<QuScore, String> implements QuScoreDao{

}
