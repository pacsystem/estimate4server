package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.entity.QuOrderby;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.QuOrderbyDao;

/**
 * 排序题 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class QuOrderbyDaoImpl extends BaseDaoImpl<QuOrderby, String> implements QuOrderbyDao{

}
