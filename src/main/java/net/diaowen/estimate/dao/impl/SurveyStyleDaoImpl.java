package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.SurveyStyleDao;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.entity.SurveyStyle;

/**
 * 问卷样式 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

@Repository
public class SurveyStyleDaoImpl extends BaseDaoImpl<SurveyStyle, String> implements SurveyStyleDao {

}
