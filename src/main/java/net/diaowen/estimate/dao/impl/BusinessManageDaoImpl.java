package net.diaowen.estimate.dao.impl;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.BusinessManageDao;
import net.diaowen.estimate.entity.BusinessManage;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class BusinessManageDaoImpl extends BaseDaoImpl<BusinessManage, String> implements BusinessManageDao {
    @Override
    public boolean checkBusinessManageExists(BusinessManage businessManage) {
        Integer count = 0;
        String id = businessManage.getId();
        if (StringUtils.isEmpty(id)) {
            String sql = "select * from t_business_manage t where t.business_name = ? and t.data_status != 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql).setParameter(1, businessManage.getBusinessName()).list();
            if(list!=null)
                count = list.size();
        } else {
            String sql = "select * from t_business_manage t where t.business_name = ? and t.id !=? and t.data_status != 1";
            List<Object[]> list = this.getSession().createSQLQuery(sql).setParameter(1, businessManage.getBusinessName()).setParameter(2, businessManage.getId()).list();
            if(list!=null)
                count = list.size();
        }

        if (count > 0) {
            return true;
        }

        return false;
    }
}
