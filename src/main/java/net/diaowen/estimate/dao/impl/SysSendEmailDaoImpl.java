package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.SysSendEmailDao;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.entity.SysSendEmail;

@Repository
public class SysSendEmailDaoImpl extends BaseDaoImpl<SysSendEmail, String> implements SysSendEmailDao {

}
