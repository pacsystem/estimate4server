package net.diaowen.estimate.dao.impl;

import net.diaowen.common.dao.BaseDaoImpl;
import net.diaowen.estimate.dao.SysOrgDao;
import net.diaowen.estimate.entity.SysOrg;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SysOrgDaoImpl extends BaseDaoImpl<SysOrg, String> implements SysOrgDao {

    @Override
    public List<SysOrg> orgSimpleList(){
        Criterion cri = Restrictions.eq("status","0");
        return super.find(cri);
    }
}
