package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.MailInviteInboxDao;
import net.diaowen.estimate.entity.MailInviteInbox;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

@Repository
public class MailInviteInboxDaoImpl extends BaseDaoImpl<MailInviteInbox, String> implements MailInviteInboxDao {

}
