package net.diaowen.estimate.dao.impl;

import net.diaowen.estimate.dao.QuCheckboxDao;
import net.diaowen.estimate.entity.QuCheckbox;
import org.springframework.stereotype.Repository;

import net.diaowen.common.dao.BaseDaoImpl;

/**
 * 多选题 dao
 * @author keyuan(keyuan258@gmail.com)
 *
 * https://github.com/wkeyuan/DWSurvey
 * http://dwsurvey.net
 */

//@Repository("quCheckboxDao")
@Repository
public class QuCheckboxDaoImpl extends BaseDaoImpl<QuCheckbox, String> implements QuCheckboxDao {

}
