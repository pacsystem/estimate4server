package net.diaowen.estimate.dao;

import net.diaowen.common.dao.BaseDao;
import net.diaowen.estimate.entity.SysOrg;

import java.util.List;

public interface SysOrgDao extends BaseDao<SysOrg, String> {

    public List<SysOrg> orgSimpleList();
}
