ALTER TABLE `estimate`.`t_survey_detail`
ADD COLUMN `congelation` int(1) NULL COMMENT '答卷冻结冻结状态 0 未冻结 1 冻结' AFTER `yn_end_time`,
ADD COLUMN `djstartime` datetime NULL COMMENT '答卷开始时间' AFTER `congelation`,
ADD COLUMN `djendtime` datetime NULL COMMENT '答卷结束时间' AFTER `djstartime`;
