-- 【产品线】评价系统
-- 【提交人】
-- 【提交时间】
-- 【脚本用途】初始化表结构
CREATE TABLE IF NOT EXISTS `t_an_answer`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '结果',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表ID',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题目ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷  问答题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_checkbox`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `other_text` varchar(3255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '复合的说明',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的结果ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷  多选题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_chen_checkbox`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的列ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 矩阵多选题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_chen_fbk`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answer_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果值',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的列ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 矩阵填空题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_chen_radio`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的列ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 矩阵单选题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_chen_score`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answser_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的结果，即分值',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的列ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 矩阵评分题' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_comp_chen_radio`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_col_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的列ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_option_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目选项ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 复合矩阵单选题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_dfillblank`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answer` varchar(3255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '答案',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目 ID',
  `qu_item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的填空项ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷  多行填空题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_enumqu`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `enum_item` int NULL DEFAULT NULL COMMENT '第几个枚举项',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目 ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷  枚举题答案' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_fillblank`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answer` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '结果',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目 ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 填空题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_order`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `ordery_num` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的结果，即分值',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 排序题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_radio`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `other_text` varchar(3255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '复合题的其它项',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目 ID',
  `qu_item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '结果选项ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 单选题保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_score`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `answser_score` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的结果，即分值',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目ID',
  `qu_row_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的行ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `exportIndex`(`belong_answer_id` ASC, `qu_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答卷 评分题' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_an_uplodfile`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件名称',
  `file_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '文件地址',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '题目 ID',
  `random_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '随机码',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '答卷 上传文件表' ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `t_an_yesno`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `belong_answer_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的答卷信息表',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问题ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示',
  `yesno_answer` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '1 是 0非',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '答案 是非题结果保存表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_business_manage`  (
  `id` varchar(128) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `business_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '业务名称',
  `remarks` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL COMMENT '备注',
  `user_id_create` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '创建用户',
  `create_time` timestamp NULL DEFAULT NULL COMMENT '创建时间',
  `user_id_modified` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NULL DEFAULT NULL COMMENT '修改用户',
  `modified_time` timestamp NULL DEFAULT NULL COMMENT '修改时间',
  `data_status` int NULL DEFAULT NULL COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_unicode_ci COMMENT = '业务管理' ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `t_common_datadict`  (
  `dict_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '数据字典编码(FK)',
  `item_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据项code码',
  `item_value` varchar(8) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据项值',
  `item_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据项名称',
  `dict_parent_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '上级编码（UUID）',
  `spell_code` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '简码',
  `data_status` tinyint NOT NULL DEFAULT 0 COMMENT '状态（默认0——正常；1—删除；2——停用；）',
  `dict_order` int NULL DEFAULT NULL COMMENT '数据项显示顺序',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `creator` varchar(21) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '创建人员',
  `modify_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  `modifier` varchar(21) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人员',
  `service_id` varchar(32) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '服务id',
  PRIMARY KEY (`dict_id`) USING BTREE,
  INDEX `idx_datadict_detail_dict_id`(`dict_id` ASC) USING BTREE,
  INDEX `idx_datadict_detail_spell_code`(`spell_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '数据字典内容信息' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_import_error`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `cell1value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的第一列内容',
  `cell2value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的第二列内容',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `db_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的数据ID',
  `file_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的导入文件名',
  `row_index` int NULL DEFAULT NULL COMMENT '对应的行号',
  `table_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的表',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  `cell1_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的第一列内容',
  `cell2_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '对应的第二列内容',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '导入错误表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_mail_invite_inbox`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '电子邮箱',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '姓名',
  `sendcloud_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sendclound返回的任务id',
  `status` int NULL DEFAULT NULL COMMENT '状态 0未发送 1已提交 2请求＝投递 3发送 4打开 5点击 100发送失败 201取消订阅 202软退信 203垃圾举报 204无效邮件',
  `survey_mail_invite_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '调查邮件邀请id',
  `us_contacts_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '我们联系信息ID',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮件邀请收件箱表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_checkbox`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `check_type` int NULL DEFAULT NULL COMMENT '说明的验证方式',
  `is_note` int NULL DEFAULT NULL COMMENT '是否带说明  0否  1是',
  `is_required_fill` int NULL DEFAULT NULL COMMENT '说明内容是否必填',
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项内容',
  `option_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项标题',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '多选题 选项表' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_chen_column`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项内容',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序号',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题ID',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_chen_option`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `option_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项问题',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_chen_row`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项问题',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_multi_fillblank`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `check_type` int NULL DEFAULT NULL COMMENT '说明的验证方式',
  `option_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项问题',
  `option_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项标题',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_orderby`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项内容',
  `option_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序号',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评分题 行选项' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_radio`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `check_type` int NULL DEFAULT NULL COMMENT '说明的验证方式',
  `is_note` int NULL DEFAULT NULL COMMENT '是否带说明  0否  1是',
  `is_required_fill` int NULL DEFAULT NULL COMMENT '说明内容是否必填',
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项内容',
  `option_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '选项标题',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单选题选项' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_qu_score`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `option_name` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '选项内容',
  `option_title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '标识',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序号',
  `qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示 0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '评分题 行选项' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_question`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `answer_input_row` int NULL DEFAULT NULL,
  `answer_input_width` int NULL DEFAULT NULL COMMENT '填空的input',
  `belong_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属问卷或题库',
  `cell_count` int NULL DEFAULT NULL COMMENT '按列显示时，列数',
  `check_type` int NULL DEFAULT NULL COMMENT '说明的验证方式',
  `contacts_attr` int NULL DEFAULT NULL COMMENT '关联到联系人属性  0不关联到联系人属性',
  `contacts_field` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关联的联系人字段',
  `copy_from_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '如果是复制的题，则有复制于那一题',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `hv` int NULL DEFAULT NULL COMMENT '水平显示 2垂直显示',
  `is_required` int NULL DEFAULT NULL COMMENT '是否必答 0非必答 1必答',
  `keywords` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '关键字',
  `order_by_id` int NULL DEFAULT NULL COMMENT '排序ID',
  `param_int01` int NULL DEFAULT NULL COMMENT '枚举题 枚举项数目 ,评分题起始分值',
  `param_int02` int NULL DEFAULT NULL COMMENT '评分题，最大分值',
  `parent_qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所属大题  只有小题才有此属性 即quTag=3的题',
  `qu_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题目名称',
  `qu_note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '题目说明',
  `qu_tag` int NULL DEFAULT NULL COMMENT '是否是大小题    1默认题  2大题  3大题下面的小题',
  `qu_title` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '题干',
  `qu_type` int NULL DEFAULT NULL COMMENT '题目类型',
  `rand_order` int NULL DEFAULT NULL COMMENT '选项随机排列  1随机排列 0不随机排列',
  `tag` int NULL DEFAULT NULL COMMENT '标记     1题库中的题   2问卷中的题',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示 0不显示   1显示',
  `yesno_option` int NULL DEFAULT NULL COMMENT '是非题的选项',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_question_bank`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `bank_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '题库名称',
  `bank_note` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '说明',
  `bank_state` int NULL DEFAULT NULL COMMENT '状态 0设计状态  1发布状态',
  `bank_tag` int NULL DEFAULT NULL COMMENT '共享题库 0 官方库   1用户共享',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `dir_type` int NULL DEFAULT NULL COMMENT '1目录，2题库',
  `excerpt_num` int NULL DEFAULT NULL COMMENT '引用次数',
  `group_id1` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷所属的组  功能分组',
  `group_id2` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '分组2	行业分组',
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '父类题库ID',
  `qu_num` int NULL DEFAULT NULL COMMENT '题目数',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  1显示 0不显示',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '题库' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_question_logic`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `cg_qu_item_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答选择题的选项ID  （0任意选项）',
  `ck_qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答选择的题ID',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `ge_le` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评分题 ge大于，le小于',
  `logic_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '逻辑类型  (1=跳转,2显示)',
  `score_num` int NULL DEFAULT NULL COMMENT '分数',
  `sk_qu_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '要跳转的题  (end1提前结束-计入结果  end2提前结束-不计结果)',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示  1显示 0不显示',
  `sk_qu_id_end` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '题目逻辑设置' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_answer`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答者是详细地址',
  `bg_an_date` datetime NULL DEFAULT NULL COMMENT '回答时间',
  `city` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答者城市',
  `complete_item_num` int NULL DEFAULT NULL COMMENT '回答的题项目数 表示有些题下面会有多重回答',
  `complete_num` int NULL DEFAULT NULL COMMENT '回答的题数',
  `data_source` int NULL DEFAULT NULL COMMENT '数据来源  0网调  1录入数据 2移动数据 3导入数据',
  `end_an_date` datetime NULL DEFAULT NULL COMMENT '回答时间',
  `handle_state` int NULL DEFAULT NULL COMMENT '审核状态  0未处理 1通过 2不通过',
  `ip_addr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答者IP',
  `is_complete` int NULL DEFAULT NULL COMMENT '是否完成  1完成',
  `is_effective` int NULL DEFAULT NULL COMMENT '是否是有效数据  1有效',
  `pc_mac` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答者MAC',
  `qu_num` int NULL DEFAULT NULL COMMENT '回答的题数',
  `survey_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷ID',
  `total_time` float NULL DEFAULT NULL COMMENT '用时',
  `user_id` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '回答者ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id_index`(`user_id` ASC) USING BTREE,
  INDEX `survey_id_index`(`survey_id` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '具体的一次调查' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_detail`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `an_item_least_num` int NULL DEFAULT NULL COMMENT '可以回答的最少选项数目',
  `an_item_most_num` int NULL DEFAULT NULL COMMENT '可以回答的最多选项数目',
  `dir_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所对应的surveyDirectory的ID',
  `effective` int NULL DEFAULT NULL COMMENT '问卷有效性 限制:1不限制, 2使用Cookie技术,3使用来源IP检测,4 每台电脑或手机只能答一次',
  `effective_ip` int NULL DEFAULT NULL COMMENT '每个IP只能答一次 1是 0否',
  `effective_time` int NULL DEFAULT NULL COMMENT '有效性间隔时间',
  `end_num` int NULL DEFAULT NULL COMMENT '收到的份数',
  `end_time` datetime NULL DEFAULT NULL COMMENT '结束时间',
  `end_type` int NULL DEFAULT NULL COMMENT '结束方式1手动结束   2依据结束时间  3依据收到的份数',
  `mail_only` int NULL DEFAULT NULL COMMENT '只有邮件邀请唯一链接的受访者可回答  1启用 0不启用',
  `refresh` int NULL DEFAULT NULL COMMENT '防刷新  1启用 0不启用',
  `refresh_num` int NULL DEFAULT NULL,
  `rule` int NULL DEFAULT NULL COMMENT '调查规则1公开, 2私有, 3令牌 表示启用访问密码',
  `rule_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '令牌',
  `show_answer_da` int NULL DEFAULT NULL,
  `show_share_survey` int NULL DEFAULT NULL COMMENT '显示分享',
  `survey_note` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL COMMENT '问卷说明',
  `survey_qu_num` int NULL DEFAULT NULL COMMENT '问卷下面有多少题目数',
  `yn_end_num` int NULL DEFAULT NULL COMMENT '是否依据收到的份数结束',
  `yn_end_time` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '具体的一次调查' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_directory`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `an_item_least_num` int NULL DEFAULT NULL,
  `answer_num` int NULL DEFAULT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `dir_type` int NULL DEFAULT NULL,
  `excerpt_num` int NULL DEFAULT NULL,
  `html_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_share` int NULL DEFAULT NULL,
  `parent_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_detail_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_model` int NULL DEFAULT NULL,
  `survey_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_qu_num` int NULL DEFAULT NULL,
  `survey_state` int NULL DEFAULT NULL,
  `survey_tag` int NULL DEFAULT NULL,
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `view_answer` int NULL DEFAULT NULL,
  `visibility` int NULL DEFAULT NULL,
  `survey_type` int NULL DEFAULT NULL,
  `orderby_num` int NULL DEFAULT NULL,
  `json_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_name_text` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `business_type_id` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '业务类型ID',
  `appraise` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '评价维度',
  `template_number` varchar(128) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '模板编号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_mail_invite`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `audit` int NULL DEFAULT NULL COMMENT '审核 0未审核  1审核通过',
  `create_date` datetime NULL DEFAULT NULL,
  `dw_send_user_mail` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发件人邮箱',
  `dw_send_user_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发件人名称',
  `dw_survey_link` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷答卷地址',
  `dw_survey_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷名称',
  `error_msg` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `fail_num` int NULL DEFAULT NULL COMMENT '发送中失败的数',
  `inbox_num` int NULL DEFAULT NULL COMMENT '总收件人数',
  `send_num` int NULL DEFAULT NULL COMMENT '已经发送的数',
  `sendcloud_msg_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `status` int NULL DEFAULT NULL COMMENT '状态 0未发送 1正在发送 2发送完成 3发送失败  4发送异常',
  `subject` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `success_num` int NULL DEFAULT NULL COMMENT '发送中成功的数',
  `survey_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '所对应的联系',
  `user_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '邮件服务' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_req_url`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `sid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `state` int NULL DEFAULT NULL,
  `survey_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_stats`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `an_avg_time` int NULL DEFAULT NULL COMMENT '回复平均用时 秒',
  `an_min_time` int NULL DEFAULT NULL COMMENT '回复最少用时 秒',
  `answer_num` int NULL DEFAULT NULL,
  `complete_num` int NULL DEFAULT NULL COMMENT '完成的数据',
  `effective_num` int NULL DEFAULT NULL COMMENT '有效数据',
  `first_answer` datetime NULL DEFAULT NULL COMMENT '第一条回答数据时间',
  `handle_pass_num` int NULL DEFAULT NULL COMMENT '通过',
  `handle_un_pass_num` int NULL DEFAULT NULL COMMENT '未通过',
  `import_num` int NULL DEFAULT NULL COMMENT '导入数据',
  `input_num` int NULL DEFAULT NULL COMMENT '录入数据',
  `is_new_data` int NULL DEFAULT NULL COMMENT '标识是否是最新数据  0不是 1是',
  `last_answer` datetime NULL DEFAULT NULL COMMENT '最后一条回答数据时间',
  `mobile_num` int NULL DEFAULT NULL COMMENT '移动数据',
  `online_num` int NULL DEFAULT NULL COMMENT '网调数据',
  `survey_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `un_complete_num` int NULL DEFAULT NULL COMMENT '未完成的数据',
  `un_effective_num` int NULL DEFAULT NULL COMMENT '无效数据',
  `un_handle_num` int NULL DEFAULT NULL COMMENT '未处理',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '单个问卷的全局统计信息' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_survey_style`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `body_bg_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '背景色',
  `body_bg_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_survey_haed` int NULL DEFAULT NULL COMMENT '是否显示问卷表头',
  `survey_bg_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷区背景色',
  `survey_bg_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷区背景图',
  `survey_content_bg_colo_topr` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_bg_color_bottom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_bg_color_middle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_bg_image_bottom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_bg_image_middle` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_bg_image_top` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_content_padding_bottom` int NULL DEFAULT NULL,
  `survey_content_padding_left` int NULL DEFAULT NULL,
  `survey_content_padding_right` int NULL DEFAULT NULL,
  `survey_content_padding_top` int NULL DEFAULT NULL,
  `survey_content_width` int NULL DEFAULT NULL,
  `survey_head_bg_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头部背景色',
  `survey_head_bg_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '头部背景图',
  `survey_head_height` int NULL DEFAULT NULL,
  `survey_head_padding_bottom` int NULL DEFAULT NULL,
  `survey_head_padding_left` int NULL DEFAULT NULL,
  `survey_head_padding_right` int NULL DEFAULT NULL,
  `survey_head_padding_top` int NULL DEFAULT NULL,
  `survey_head_width` int NULL DEFAULT NULL COMMENT '头部宽',
  `survey_id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_padding_bottom` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_padding_left` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_padding_right` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_padding_top` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '问卷内边距',
  `survey_style_type` int NULL DEFAULT NULL COMMENT '0属于某个问卷的 、1 样式模板',
  `survey_width` int NULL DEFAULT NULL COMMENT '问卷宽度',
  `show_body_bi` int NULL DEFAULT NULL,
  `show_survey_bi` int NULL DEFAULT NULL,
  `show_survey_cbim` int NULL DEFAULT NULL,
  `show_survey_hbgi` int NULL DEFAULT NULL,
  `survey_content_bg_color_top` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_survey_logo` int NULL DEFAULT NULL,
  `survey_logo_image` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'logo图片',
  `question_option_text_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `question_title_text_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_note_text_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_title_text_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `survey_btn_bg_color` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '按钮背景色',
  `show_sur_note` int NULL DEFAULT NULL COMMENT '显示问卷副标题',
  `show_sur_title` int NULL DEFAULT NULL COMMENT '显示问卷标题',
  `show_progressbar` int NULL DEFAULT NULL COMMENT '显示答题进度',
  `show_ti_num` int NULL DEFAULT NULL COMMENT '显示题序号',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `t_sys_db_backup`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `backup_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备份名称',
  `backup_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备份文件路径',
  `create_date` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `des` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用作备分' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_sys_dict_item`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `dict_type_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典类型编码',
  `dict_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据编码',
  `dict_value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '值',
  `dict_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '字典名称',
  `dict_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` int NULL DEFAULT NULL COMMENT '排序',
  `status` enum('0','1') CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '1' COMMENT '0 禁用；1 启用',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_by_user_id` int NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_by_user_id` int NULL DEFAULT NULL COMMENT '修改人id',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_dict_code`(`dict_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统字典条目表' ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `t_sys_dict_type`  (
  `id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '主键',
  `dict_type_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据类型编码',
  `dict_type_parent_ode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据父类类型编码',
  `dict_type_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '数据类型名称',
  `dict_desc` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '描述',
  `sort` int NULL DEFAULT 1 COMMENT '排序',
  `create_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建人',
  `create_by_user_id` int NULL DEFAULT NULL COMMENT '创建人id',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_by` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '修改人',
  `update_by_user_id` int NULL DEFAULT NULL COMMENT '修改人id',
  `update_time` datetime NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `idx_dict_type_code`(`dict_type_code` ASC) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统字典类型表' ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `t_sys_email`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `create_date` datetime NULL DEFAULT NULL,
  `is_check` int NULL DEFAULT NULL COMMENT '是否验证',
  `post` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'POST',
  `send_email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '发送邮箱',
  `state` int NULL DEFAULT NULL COMMENT '状态',
  `stmp_pwd` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱密码',
  `stmp_server` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'STMP服务器',
  `stmp_user` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱用户',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '系统邮件' ROW_FORMAT = DYNAMIC;

CREATE TABLE IF NOT EXISTS `t_user`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '激活账号CODE',
  `birthday` datetime NULL DEFAULT NULL COMMENT '出生年月',
  `cellphone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '手机',
  `create_by` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '创建者',
  `create_time` datetime NULL DEFAULT NULL COMMENT '创建时间',
  `edu_quali` int NULL DEFAULT NULL COMMENT '最高学历',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '邮箱',
  `find_pwd_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '找回密码code',
  `find_pwd_last_date` datetime NULL DEFAULT NULL COMMENT '找回密码最后期限  默认设置一天之内',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '最后一次登录时间',
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '用户名',
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '加点盐',
  `sex` int NULL DEFAULT NULL COMMENT '性别',
  `sha_password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '密码',
  `status` int NULL DEFAULT NULL COMMENT '账号状态2激活 1未激活 0不可用',
  `version` int NULL DEFAULT NULL COMMENT '1 默认 2测试',
  `avatar` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '虚拟头像',
  `session_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '会话ID',
  `sha_password_temp` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '临时密码',
  `visibility` int NULL DEFAULT NULL COMMENT '是否显示 0不显示',
  `wwwooo` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `wx_open_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '微信OPENID',
  `role_id` varchar(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '角色',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `login_name`(`login_name`) USING BTREE
) ENGINE = MyISAM AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户表' ROW_FORMAT = Dynamic;

CREATE TABLE IF NOT EXISTS `tracker`  (
  `id` varchar(55) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT '' COMMENT '主键ID',
  `data_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据ID',
  `data_type` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '数据类型',
  `login_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '登录用户名',
  `operation` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '操作',
  `optime` datetime NULL DEFAULT NULL COMMENT '操作时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '追踪或行为记录表' ROW_FORMAT = DYNAMIC;

SET FOREIGN_KEY_CHECKS = 1;
